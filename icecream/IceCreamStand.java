package icecream;

import java.util.Scanner;

public class IceCreamStand
{

    public static void main (String[] args)
    {
    
        int numScopes;
        double subtotal;
        double total;
          
        final double tax = 0.15;
        final double pricePerScope = 1.5;
        final double pricePerCone = 0.45;
      
        Scanner input = new Scanner(System.in);
        
        System.out.println("Enter Number of Scopes[1-3]:");
        numScopes = input.nextInt();
        
        while(numScopes == 0 || numScopes > 3)
        {
            
           System.out.println("only numbers ranging 1-3 are valid");
           System.out.println("Enter Number of Scopes[1-3]:");
           numScopes = input.nextInt();
           
        }
        
    }
}