import java.util.HashMap;
import java.util.Map;

/**
   This program calculates the value of an expression 
   consisting of numbers, arithmetic operators, and parentheses.  
   
   @editedBy: Gideon Landry
   @since April 2, 2020
*/
public class ExpressionCalculator
{
   public static void main(String[] args)
   {
      Map<String, Number> vars = new HashMap<>();

      //Test Case 0
      String input0 = "1 + 2 * 3"; //7
      input0 = input0.replaceAll(" ",""); // eliminate spaces
      Evaluator e0 = new Evaluator(input0);
      int value0 = e0.getExpressionValue();
      System.out.println(input0 + "=" + value0);

      
      //Test Case 1
      String input1 = "1 + 2 * 7 % 3"; //3
      input1 = input1.replaceAll(" ",""); // eliminate spaces
      Evaluator e1 = new Evaluator(input1);
      int value1 = e1.getExpressionValue();
      System.out.println(input1 + "=" + value1);

      //Test Case 2
      String input2 = "1 + 2 * 3 ^ 2 ^ 3"; //13123
      input2 = input2.replaceAll(" ",""); // eliminate spaces
      Evaluator e2 = new Evaluator(input2);
      int value2 = e2.getExpressionValue();
      System.out.println(input2 + "=" + value2);

      //Test Case 3
      vars.put("a", 5); //variable
      vars.put("b", 3); //variable
      String input3 = "a + b ^ 2 * 3"; //32
      input3 = input3.replaceAll(" ",""); // eliminate spaces
      Evaluator e3 = new Evaluator(input3, vars);
      int value3 = e3.getExpressionValue();
      System.out.println(input3 + "=" + value3);
      
      // Gideon: Test Case 4.
      vars.clear();
      vars.put("a", 6);
      vars.put("b", 8);
      vars.put("c", 4);
      vars.put("d", 5);
      String input4 = "a + b * c ^ d * (7 * 8 % 5)"; // 8198
      input4 = input4.replaceAll(" ","");
      Evaluator e4 = new Evaluator(input4,vars);
      int value4 = e4.getExpressionValue();
      System.out.println(input4 + " = " + value4); 
      
      // Gideon: Test Case 5.
      vars.clear();
      vars.put("a", 2);
      vars.put("b", 3);
      vars.put("d", 5);
      String input5 = "a ^ b ^ a * d"; // 2560
      input5 = input5.replaceAll(" ","");
      Evaluator e5 = new Evaluator(input5,vars);
      int value5 = e5.getExpressionValue();
      System.out.println(input5 + " = " + value5);
    }
}
