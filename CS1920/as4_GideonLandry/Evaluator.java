import java.util.Map;
import java.util.HashMap;

/**
   A class that can compute the value of an arithmetic expression.
   
   @editedBy: Gideon Landry
   @since April 2, 2020
*/
public class Evaluator
{
   private ExpressionTokenizer tokenizer;

   Map<String, Number> vars = new HashMap<>();
   
   /**
      Constructs an evaluator.
      @param anExpression a string containing the expression
      to be evaluated
   */
   public Evaluator(String anExpression)
   {
      tokenizer = new ExpressionTokenizer(anExpression);
   }

   /**
    * Gideon: this constructor will take in math variables as an added paramater
    * 
    * @param anExpression  a string containing the expression
    * @param vars  a Map containing all variables(the Map keys) and their respective values.
    */
   public Evaluator (String anExpression, Map<String, Number> vars)
   {
       
       tokenizer = new ExpressionTokenizer(anExpression);
       
       // Gideon: add variables to this object.
       this.vars = vars;

   }
   
   /**
      Evaluates the expression.
      @return the value of the expression.
   */
   public int getExpressionValue()
   {
      int value = getTermValue();
      boolean done = false;
      while (!done)
      {
         String next = tokenizer.peekToken();
         if ("+".equals(next) || "-".equals(next))
         {
            tokenizer.nextToken(); // Discard "+" or "-"
            int value2 = getTermValue();
            if ("+".equals(next)) { value = value + value2; }
            else { value = value - value2; }
         }
         else 
         {
            done = true;
         }
      }
      return value;
   }

   /**
    * 
    *  Evaluates the next term found in the expression.
    *  @return the value of the term
    *  
    *  Gideon: added modulo functionality
    *  
    */
   public int getTermValue()
   {
      int value = getFactorValue();
      boolean done = false;
      while (!done)
      {
         String next = tokenizer.peekToken();
         if ("*".equals(next) || "/".equals(next) || "%".equals(next))
         {
            tokenizer.nextToken();
            int value2 = getFactorValue();
            if ("*".equals(next)) 
            { 
                value = value * value2; 
            }
            else if ("/".equals(next)) 
            { 
                value = value / value2;
            }
            else 
            {
                value = value % value2;
            }
         }
         else 
         {
            done = true;
         }
      }
      return value;
   }

   /**
      Evaluates the next factor found in the expression.
      @return the value of the factor
      
      Gideon: added variable and exponent processing.
   */
   public int getFactorValue()
   {
      int value;
      String next = tokenizer.peekToken();
  
      if ("(".equals(next))
      {
         tokenizer.nextToken(); // Discard "("
         value = getExpressionValue();
         tokenizer.nextToken(); // Discard ")"
      }
      
      // Gideon: if next value is a variable replace variable with respective value from vars HashMap .
      else if(vars.containsKey(next))
      {

          value = vars.get(next).intValue();
          tokenizer.nextToken();
          
      }
      else
      {
         value = Integer.parseInt(tokenizer.nextToken());
      }
      
      // Gideon: if next symbol is caret (exponent symbol) perform exponential operation.
      if("^".equals(tokenizer.peekToken()))
      {
         // Gideon: discard "^".
         tokenizer.nextToken();
         
         // Gideon: get next factor.
         int value2 = getFactorValue();
         
         // Gideon: perform exponential operation recursively.
         value = getPowValue(value, value2);
         
      }
      return value;
   }
   
   /**
    * Gideon: this recursive method will raise value to the power of value2.
    * 
    * @param value  the base.
    * @param value2  the exponent (or the number of times to multiply the base by itself).
    * 
    * @return the value of power operation.
    */
   public int getPowValue(int value, int value2)
   {
       
       // Gideon: if value2 is at least 1 or more continue exponential operation through recursion.
       if(value2 > 0)
           return value * getPowValue(value,(value2 - 1));
       
       return 1;
   }
   
}
