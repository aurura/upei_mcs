import java.util.ArrayList;

/**
 * 
 *
 * @author Gideon Landry
 * @version March 5, 2020
 */
public class Lot implements Winnable, Reportable
{

    private int lotNum;
    private int state;
    
    private double reserve;

    private String lotName;
    private String description;

    ArrayList<Bid> bids = new ArrayList<Bid>();

    /**
     * Constructor for objects of class Lot
     */
    public Lot()
    {

        lotNum = 0;
        lotName = "";
        description = "";
        state = 0;

    }

    /**
     * 
     * 
     * @param lotID  is the lotNum of new lot.
     * @param name  the name of the new lot.
     * @param descr  the description of the new lot.
     * @state
     */
    public Lot(int lotID, String lotName, String description, int state)
    {

        this.lotNum = lotID;
        this.lotName = lotName;
        this.description = description;
        this.state = state;

    }

    /**
     * @param lotID  new lot ID
     */
    public void setLotNum(int lotID)
    {
        
        this.lotNum = lotID;
    
    }
    
    /**
     * @param reserve
     */
    public void setReserve(double reserve)
    {
        
        this.reserve = reserve;
        
    }
    
    /**
     * @param lotName  new lotName
     */
    public void setLotName(String lotName)
    {
        
        this.lotName = lotName;
        
    }
    
    /**
     * @param description  new description
     */
    public void setDescription(String description)
    {
        
        this.description = description;
        
    }
    
    /**
     * @param state  state to replace current state.
     */
    public void setState(int state)
    {
        
        this.state = state;
        
    }
    
    /**
     * @return the lot ID.
     */
    public int getLotNum()
    {

        return lotNum;

    }

    /**
     * @return the lot reserve
     */
    public double getReserve()
    {
        
        return reserve;
        
    }
    
    /**
     * @return the lot name
     */
    public String getLotName()
    {
        
        return lotName;
        
    }
    
    /**
     * @return the description of the lot.
     */
    public String getDescription()
    {
        
        return description;
        
    }
    
    /**
     * @return the state of lot.
     */
    public int getState()
    {
        
        return state;
        
    }
    
    /**
     * add a new bidder to lot.
     */
    public void addBid(Bid bid)
    {

        // check if last bidder has the same name as this one.
        if(bids.size() > 0 && bids.get(bids.size() - 1).getBidder().equals(bid.getBidder()))
        {
            // check if last bid is smaller then this one.
            if(bids.get(bids.size() - 1).getBid() < bid.getBid())

            // replace last bid amount to new amount.
                bids.get(bids.size() - 1).setBid(bid.getBid());

        }

        else

            bids.add(bid);

    }

    /**
     * 
     */
    public Bid getWinningBid()
    {

        // if there are no bids return empty bid.
        if(bids.isEmpty())
            return new Bid();

        double highBid = 0;
        int bidID = 0;

        for(int i = 0; i < bids.size(); i++)
        {

            if(bids.get(i).getBid() > highBid)
            {

                highBid = bids.get(i).getBid();
                bidID = i;

            }

        }

        return bids.get(bidID);
    }

    /**
     * this methode will remove all bids.
     */
    public void withdrawBids()
    {
        
        bids.clear();
        
    }
    
    /**
     * this method will remove bids from bidder.
     * 
     * @param name  the name of the bidder.
     * 
     */
    public void withdrawBids(String name)
    {

        if(!bids.isEmpty())
            for(int i = 0; i < bids.size(); i++)
            {

                if(bids.get(i).getBidder().equals(name))
                    bids.remove(i);

            }

    }

    /**
     * Report information regarding the class
     */
    public void report()
    {

        System.out.println("Current bidding for: Lot: " + lotNum + " Name: " + lotName + " Description: " + description);
        if(bids.isEmpty())

            System.out.println("There are no bids for this auction.");

        else

            for(Bid bid : bids)
            {

                bid.report();

            }

    }

    /**
     * Report the winner information.
     */
    public void reportWinner()
    {

        Bid temp = getWinningBid();

        System.out.println("Winning Bidder: " + temp.getBidder() + " Bid: " + temp.getBid());

    }

    public int getBidCount()
    {

        return bids.size();

    }
}
