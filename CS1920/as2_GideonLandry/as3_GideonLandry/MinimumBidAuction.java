
/**
 * minimum bid auction class
 *
 * @author Gideon Landry
 * @version March 5, 2020
 */
public class MinimumBidAuction extends Auction
{

    private double minBid;

    /**
     * Default Constructor
     * 
     * @param auctionID  the unique ID for new auction.
     */
    public MinimumBidAuction(int auctionID)
    {

        super(auctionID);
        this.minBid = 0;

    }

    /**
     * 
     * @param auctionID  the unique ID for new auction.
     * @param minBid  the minimum bid amount accepted.
     */
    public MinimumBidAuction(int auctionID, double minBid)
    {

        super(auctionID);
        this.minBid = minBid;

    }

    // setters
    
    /**
     * @param minBid the new minimum bid raise.
     */
    public void setMinBid(double minBid)
    {
        
        this.minBid = minBid;
        
    }
    
    // getters
    
    /*
     * @return minBid
     */
    public double getMinBid()
    {
        
        return minBid;
        
    }
    
    /**
     * add a bid to requested lot.
     * 
     * @param num  the lot number for which to add bid.
     * @param name  the name of the bidder.
     * @param amount  the amount the bidder is bidding.
     * 
     * @return true if auction is open, false if auction is closed.
     */
    public boolean placeBid(int lotID, String name, double amount)
    {
        
        // check if new bid is at least minBid greater then current highest bid, if so add and return success result..
        if((amount - super.getWinningBid(lotID).getBid()) >= 50)
            return super.placeBid(lotID, name, amount);

        return false;

    }

    /**
     * Report information regarding the class
     */
    public void report()
    {

        super.report();

    }
}
