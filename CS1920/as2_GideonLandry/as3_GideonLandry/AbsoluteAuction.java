
/**
 * absolute auction class
 *
 * @author Gideon Landry
 * @version March 5, 2020
 */
public class AbsoluteAuction extends Auction
{
    // instance variables - replace the example below with your own
    private int num;  

    public AbsoluteAuction(int auctionID)
    {

        super(auctionID);

    }

    /**
     * add a bid to requested lot.
     * 
     * @param num  the lot number for which to add bid.
     * @param name  the name of the bidder.
     * @param amount  the amount the bidder is bidding.
     * 
     * @return true if auction is open, false if auction is closed.
     */
    public boolean placeBid(int lotID, String name, double amount)
    {

        return super.placeBid(lotID, name, amount);

    }   

    /**
     * Report information regarding the class
     */
    public void report()
    {

        super.report();

    }
}
