import java.util.ArrayList;

/**
 * reserve auction class.
 *
 * @author Gideon Landry
 * @version March 5, 2020
 */
public class ReserveAuction extends Auction
{

    public ReserveAuction(int auctionID)
    {
        
        super(auctionID);
        
    }
    
    /**
     * add a new lot to lots ArrayList.
     * 
     * @param lot  an object of Lot.
     */
    public void addLot(Lot lot, double reserve)
    {

        if(reserve > 0)
        {

            lot.setReserve(reserve);
            super.addLot(lot);

        }
    }

    /**
     * this method will close the auction.
     * 
     * @return not (!) value of open.
     */
    public boolean close()
    {

        boolean reserveMet = false;
        
        for(Lot lot : this.getLots())
            if(lot.getWinningBid().getBid() >= lot.getReserve())
                reserveMet = true;

        if(!reserveMet)        
            this.clearAllBids();

        return super.close();  

    }

    /**
     * Report information regarding the class
     */
    public void report()
    {

        super.report();

    }

}
