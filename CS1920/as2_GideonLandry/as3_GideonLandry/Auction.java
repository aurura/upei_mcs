import java.util.ArrayList;

/**
 * Abstract auction class used by all sub auction classes.
 *
 * @author Gideon Landry
 * @version March 5, 2020
 */
public abstract class Auction implements Reportable
{

    private int auctionID;

    private boolean open;

    private ArrayList<Lot> lots = new ArrayList<Lot>();

    public Auction(int auctionID)
    {

        this.auctionID = auctionID;

    }

    /**
     * @param auctionID  ID to replace current auction ID.
     */
    public void setAuctionID(int auctionID)
    {
        
        this.auctionID = auctionID;
        
    }
    
    /**
     * @return the auction ID.
     */
    public int getAuctionID()
    {

        return auctionID;

    }

    /**
     * @return the open status.
     */
    public boolean getOpen()
    {

        return open;

    }
    
    /**
     * @return Array List of lots.
     */
    public ArrayList<Lot> getLots()
    {
        
        return lots;
        
    }
    
    /**
     * This will return a lot with given id.
     * 
     * @param lotID  the ID of the lot being requested.
     * 
     * @return requested lot or empty lot if not found.
     */
    public Lot getLotByID(int lotID)
    {

        // look for lot with ID provided.
        for(Lot lot : lots)
            if(lot.getLotNum() == lotID)
                return lot;

        // if no lot is found matching the lot ID provided return empty lot.
        return new Lot();

    }
    
    /**
     * This will return a lot with given id.
     * 
     * @param state  the state of the lot being requested.
     * 
     * @return requested lot or empty lot if not found.
     */
    public Lot getLotByState(int state)
    {

        // look for lot with ID provided.
        for(Lot lot : lots)
            if(lot.getState() == state)
                return lot;

        // if no lot is found matching the lot ID provided return empty lot.
        return new Lot();

    }
    
    /**
     * this method will provid the winning bid
     * 
     * @return winning bid
     */
    public Bid getWinningBid(int lotID)
    {

        if(lots.isEmpty())
            return new Bid();

        for(int i = 0; i < lots.size(); i++)
        {

            if(lots.get(i).getLotNum() == lotID)
                return lots.get(i).getWinningBid();

        }

        return new Bid();

    }

    /**
     * @return the number of lots in this auction.
     */
    public int getLotCount()
    {

        return lots.size();

    }

    /**
     * get the number of bids for lot with ID given
     * 
     * @param lotID  the ID for the lot being requested.
     * 
     * @return the number of bids in lot.
     */
    public int getBidCount(int lotID)
    {

        int num = 0;

        for(Lot lot : lots)
            if(lot.getLotNum() == lotID)
                num = lot.getBidCount();

        return num;

    }
    
    public void clearAllBids()
    {
        
        for(Lot lot : lots)
            lot.withdrawBids();
        
    }
    
    /**
     * add a new lot to lots ArrayList.
     * 
     * @param lot  an object of Lot.
     */
    public void addLot(Lot lot)
    {

        lots.add(lot);

    }

    /**
     * this method will open the auction.
     * 
     * @return value of open.
     */
    public boolean open()
    {

        open = true;

        for(Lot lot : lots)
            if(lot.getBidCount() != 0)
                open = false;

        return open;   

    }

    /**
     * this method will close the auction.
     * 
     * @return not (!) value of open.
     */
    public boolean close()
    {

        open = false;

        return !open;   

    }

    /**
     * add a bid to requested lot.
     * 
     * @param num  the lot number for which to add bid.
     * @param name  the name of the bidder.
     * @param amount  the amount the bidder is bidding.
     * 
     * @return true if auction is open, false if auction is closed.
     */
    public boolean placeBid(int lotID, String name, double amount)
    {

        if(open)
        {

            //if(lots.get(lotID).
            Bid bid = new Bid(name, amount);

            for(Lot lot : lots)
            {
                if(lot.getLotNum() == lotID)
                {

                    lot.addBid(bid);

                    return true;

                }
            }

            return false;

        }

        return false;

    }   

    public void report()
    {
        // check if auction is open
        if(open)
            System.out.println("Auction: " + auctionID + " is currently open and accepting bids.");

        else
            System.out.println("Auction: " + auctionID + " is currently closed for bidding.");

        // check if there are any lots. if not 
        if(lots.isEmpty())
            System.out.println("There are currently no lots.");
        // report all lots.    
        else
            for(Lot lot : lots)
            {

                lot.report();

                if(!open && lot.getBidCount() > 0)
                    lot.reportWinner();

            }
    }
}
