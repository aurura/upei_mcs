
/**
 * An interface that will be implemented by the Auction class and by extension all classes that extend it.
 *
 * @author Gideon Landry
 * @version March 8, 2020
 */
public interface Biddable
{
    /**
     * Removes all bids from a particular lot.
     * 
     * @param lotID  The unique lot number in the auction
     */
    void clearBids(int lotID);
    
    /**
     * Retrieves the number of all bids made for a particular auction lot.
     * 
     * @param lotID  The unique lot number in the auction
     * 
     * @return The number of bids made on the auction.
     */
    int getBidCount(int lotID);
    
    /**
     * Retrieves the number of bid be a particular bidder in an auction lot.
     * 
     * @param lotID  The unique lot number in the auction.
     * @param InBidder  The bidder name to search for in the bid list.
     * 
     * @return The number of bids made on the auction by the bidder.
     */
    int getBidCount(int lotID, java.lang.String inBidder);
    
    /**
     * Retrieves the winning bid from the auction lot identified by the first parameter.
     * 
     * @param lotID  The unique lot number in the auction
     * 
     * @return The winning Bid; if there are no bids it returns an empty bid.
     */
    Bid getWinningBid(int lotID);
    
    /**
     *  Place a bid on a lot.
     *  
     * @param lotID  The unique lot number in the auction
     * @param inBid  The bid to be placed containing both the bidder name and the bid value.
     * 
     * @return True if the bid has been made succesfully. False if the bid does not get made either because the auction is not open or the bid does not follow the rules of the auction.
     */
    boolean placeBid(int lotID, Bid inBid);
    
    /**
     * Place a bid, by a bidder on a lot.
     * 
     * @param lotID  The unique lot number in the auction
     * @param inBidder  The name of the bidder making the bid.
     * @param inBid  The value of the bid to make on the item.
     * 
     * @return True if the bid has been made successfully. False if the bid does not get made either because the auction is not open or the bid does not follow the rules of the auction.
     */
    boolean placeBid(int lotID, java.lang.String inBidder, double inBid);
    
    /**
     * Removes all of the bids by a particular bidder.
     * 
     * @param lotID  The unique lot number in the auction
     * @param inBidder  The name of the bidder whose bids should be removed.
     * 
     * @return The number of bids removed.
     */
    int withdrawBid(int lotID, java.lang.String inBidder);

}
