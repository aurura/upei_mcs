
/**
 * A class for testing different auction rules 
 * 
 * modified By Gideon Landry
 * @version March 9, 2020
 * 
 */
public class AuctionTester {

    /** A method to test the implementation of the Minimum Bid Auction */
    public void testMinimumBidAuction()
    {

        MinimumBidAuction minimumTest = new MinimumBidAuction(1,50);
        System.out.println("Test 7: Expect minimum bid to be 50");

        if(minimumTest.getMinBid() == 50)
            System.out.println("Success 7");
        else
            System.out.println("Failure 7");

        System.out.println("Test 8: Add 2 lots");
        Lot minimum1 = new Lot(1, "lot1", "first lot of minimum bid auction.",0);
        Lot minimum2 = new Lot(2, "lot2", "second lot of minimum bid auction.",0);

        minimumTest.addLot(minimum1);
        minimumTest.addLot(minimum2);

        if(minimumTest.getLotCount() == 2)
            System.out.println("Success 8");
        else
            System.out.println("failure 8");

        minimumTest.open();

        System.out.println("Test 9: Expect bids less then minimum bid to not be added.");

        minimumTest.placeBid(1, "Lex Luthor", 1.00);

        if(minimumTest.getBidCount(1) == 0)
            System.out.println("Success 9");
        else
            System.out.println("failure 9");

        System.out.println("Test 10: Expect bids that meat minimum bid requirment to be added.");

        minimumTest.placeBid(1, "Lena Luthor", 50);

        if(minimumTest.getBidCount(1) == 1)
            System.out.println("Success 10");
        else
            System.out.println("failure 10");

        System.out.println("Test 11: Expect 2 of 3 bids to be added.");

        minimumTest.placeBid(1, "Lex Luthor", 100);
        minimumTest.placeBid(1, "Clark Kent", 110);
        minimumTest.placeBid(1, "Lana Lang", 159);

        if(minimumTest.getBidCount(1) == 3)
            System.out.println("Success 11");
        else
            System.out.println("failure 11");

        System.out.println("Test 12: Expect proper reports");

        minimumTest.close();
        
        minimumTest.report();

    }

    /** A method to test the implementation of the Sealed Auction */
    public void testSealedAuction()
    {

        Lot sealed1 = new Lot(1, "lot1", "first lot of sealed auction.",0);

        SealedAuction sealedTest = new SealedAuction(1);

        System.out.println("Test 1: Expect 1 lot Added");

        sealedTest.addLot(sealed1);

        if(sealedTest.getLotCount() == 1)
            System.out.println("Success 1");
        else
            System.out.println("Failure 1");

        sealedTest.open();

        System.out.println("Test 2: Add 4 bids");
        sealedTest.placeBid(1, "Lex Luthor", 1.00);
        sealedTest.placeBid(1, "Lena Luthor", 2.00);
        sealedTest.placeBid(1, "Clark Kent", 1.50);
        sealedTest.placeBid(1, "Lana Lang", 2.50);

        if(sealedTest.getLotByID(1).getBidCount() == 4)
            System.out.println("Success 2");
        else
            System.out.println("failure 2");

        System.out.println("Test 3: Expect no bids to be revealed while auction is open");

        Bid testBid = sealedTest.getWinningBid(1);

        if(testBid.getBid() == 0 && testBid.getBidder().equals(""))
            System.out.println("Success 3");
        else
            System.out.println("failure 3");

        System.out.println("Test 4: Expect withdrawal of bids.");

        sealedTest.getLotByID(1).withdrawBids("Lex Luthor");

        if(sealedTest.getBidCount(1) == 3)
            System.out.println("Success 4");
        else
            System.out.println("failure 4");

        System.out.println("Test 5: Expect 1 bid from each bidder.");

        sealedTest.placeBid(1, "Clark Kent", 1.50);

        if(sealedTest.getBidCount(1) == 3)
            System.out.println("Success 5");
        else
            System.out.println("failure 5");

        System.out.println("Test 6: Expect proper reports");

        sealedTest.close();
        
        sealedTest.report();

    }

    /** A method to test the implementation of the Reserve Auction */
    public void testReserveAuction()
    {

        ReserveAuction reserveTest = new ReserveAuction(1);
        System.out.println("Test 13: Expect lots with reserves equal to 0 to be rejected.");

        Lot minimum1 = new Lot(1, "lot1", "first lot of minimum bid auction.",0);
        Lot minimum2 = new Lot(2, "lot2", "second lot of minimum bid auction.",0);

        reserveTest.addLot(minimum1,100);
        reserveTest.addLot(minimum2,0);

        if(reserveTest.getLotCount() == 1)
            System.out.println("Success 13");
        else
            System.out.println("Failure 13");

        System.out.println("Test 14: Expect 3 bids below reserve to be added");

        reserveTest.open();

        reserveTest.placeBid(1, "Lena Luthor", 5);
        reserveTest.placeBid(1, "Clark Kent", 40);
        reserveTest.placeBid(1, "Lana Lang", 44);

        if(reserveTest.getBidCount(1) == 3)
            System.out.println("Success 14");
        else
            System.out.println("failure 14");

        System.out.println("Test 15: Expect no reported winners.");

        reserveTest.close();

        if(reserveTest.getWinningBid(1).getBid() == 0)
            System.out.println("Success 15");
        else
            System.out.println("failure 15");

        System.out.println("Test 16: Expect bids to be cleared after close");

        if(reserveTest.getBidCount(1) == 0)
            System.out.println("Success 16");
        else
            System.out.println("failure 16");

        System.out.println("Test 17: Expect winning bidder.");

        reserveTest.open();

        reserveTest.placeBid(1, "Lena Luthor", 5);
        reserveTest.placeBid(1, "Clark Kent", 40);
        reserveTest.placeBid(1, "Lana Lang", 100);

        if(reserveTest.getWinningBid(1).getBid() == 100)
            System.out.println("Success 17");
        else
            System.out.println("failure 17");

        System.out.println("Test 18: Expect proper reports");

        reserveTest.close();
        
        reserveTest.report();

    }

    /** A method to test the implementation of the Absolute Auction */
    public void testAbsoluteAuction() {
        Lot action1 = new Lot(1, "Action Comics 1", "First appearance of Superman.",0);
        Lot detective27 = new Lot(2, "Detective Comics 27", "First appearance of Batman.",0);
        Lot allstar8 = new Lot(3, "All Star Comics 8", "First appearance of Wonder Woman.",0);

        AbsoluteAuction comicAuction = new AbsoluteAuction(1);

        System.out.println("Test: Expect 3 Lots Added");
        comicAuction.addLot(action1);
        comicAuction.addLot(detective27);
        comicAuction.addLot(allstar8);
        if(comicAuction.getLotCount() == 3)
            System.out.println("Success: 3 lots added correctly.");
        else
            System.out.println("Failure: 3 lots added incorrectly.");

        System.out.println("Test Manual Check: Report Auction closed, 3 lots, no bids");
        comicAuction.report();

        System.out.println("Test: Place Bid on closed");
        if(comicAuction.placeBid(1, "Lex Luthor", 0.50))
        {
            System.out.println("Failure: Bid placed on closed auction.");
        }
        else
        {
            System.out.println("Success: Bid failed on closed auction.");
        }

        System.out.println("Test: Successful open");
        if(comicAuction.open())
            System.out.println("Success: Auction opened successfully.");
        else
            System.out.println("Failure: Auction not opened.");

        System.out.println("Test: Add 4 bids, 1 failed bid, Lex Luthor with bid of 2.50 highest.");
        comicAuction.placeBid(1, "Lex Luthor", 1.00);
        comicAuction.placeBid(1, "Lena Luthor", 2.00);
        comicAuction.placeBid(1,"Lex Luthor", 1.50);
        comicAuction.placeBid(1, "Lex Luthor", 2.50);

        if(comicAuction.getLotByID(1).getBidCount() == 3)
        {
            System.out.println("Success: 3 bids added.");
            if((comicAuction.getWinningBid(1).getBid() == 2.50)&& comicAuction.getWinningBid(1).getBidder().equals("Lex Luthor"))
            {
                System.out.println("Success: Lex Luthor is highest bidder with bid 2.50.");
            }
            else
            {
                System.out.println("Failure: Incorrect highest bid.");
            }
        }
        else if (comicAuction.getLotByID(1).getBidCount() > 3){
            System.out.println("Failure: More than 3 bids added.");
        }
        else
        {
            System.out.println("Failures: Less than 3 bids added.");
        }

        System.out.println("Test: Manual check report Auction open, 3 lots, first lot with 3 bids");
        comicAuction.report();

        System.out.println("Test: Successful close.");
        if(comicAuction.close())
            System.out.println("Success: Auction closed successfully.");
        else
            System.out.println("Failure: Auction not closed.");

        System.out.println("Test: Prevent reopening after bids.");
        if(comicAuction.open())
        {
            System.out.println("Failure: Auction reopened after bidding.");
        }
        else
        {
            System.out.println("Success: Auction remained closed after bidding");
        }

        System.out.println("Test: Place Bid on closed after bidding occurred.");
        if(comicAuction.placeBid(2, "Edward Nigma", 1.00))
        {
            System.out.println("Failure: Bid placed on closed auction.");
        }
        else
        {
            System.out.println("Success: Bid failed on closed auction.");
        }

        System.out.println("Test: Manual check report Auction closed, 3 lots, first lot with 3 bids");
        comicAuction.report();

        System.out.println("Test: Remove bids by Lex Luthor.");
        comicAuction.getLotByID(1).withdrawBids("Lex Luthor");
        if(comicAuction.getLotByID(1).getBidCount() == 1)
        {
            System.out.println("Success: 2 bids removed.");
            if((comicAuction.getWinningBid(1).getBid() == 2.00)&& comicAuction.getWinningBid(1).getBidder().equals("Lena Luthor"))
            {
                System.out.println("Success: Lena Luthor is highest bidder with bid 2.00.");
            }
            else
            {
                System.out.println("Failure: Incorrect highest bid.");
            }
        }
        else
        {
            System.out.println("Failures: Incorrect number of bids removed.");
        }

        System.out.println("Test: Manual check report Auction closed, 3 lots, first lot with 1 bids");
        comicAuction.report();

        comicAuction.getLotByID(1).withdrawBids("Lena Luthor");
        if(comicAuction.getBidCount(1) == 0) {
            System.out.println("Success: Final bid removed.");
        }
        else
        {
            System.out.println("Failures: Final bid not removed.");
        }

        System.out.println("Test: Manual check report Auction closed, 3 lots, no bids.");
        comicAuction.report();

        System.out.println("Test: Successful open after all bids removed.");
        if(comicAuction.open())
            System.out.println("Success: Auction opened successfully.");
        else
            System.out.println("Failure: Auction not opened.");

    }

    public static void main(String args[]) {

        AuctionTester auctions = new AuctionTester();

        System.out.println("=====================");
        System.out.println("Testing Absolute Auction");
        System.out.println("=====================");

        auctions.testAbsoluteAuction();

        System.out.println("=====================");
        System.out.println("Testing Sealed Auction");
        System.out.println("=====================");
        auctions.testSealedAuction();

        System.out.println("=====================");
        System.out.println("Testing Reserve Auction");
        System.out.println("=====================");
        auctions.testReserveAuction();

        System.out.println("=====================");
        System.out.println("Testing Minimum Bid Auction");
        System.out.println("=====================");
        auctions.testMinimumBidAuction();

    }
}