
/**
 * sealed auction class.
 *
 * @author Gideon Landry
 * @version March 5, 2020
 */
public class SealedAuction extends Auction
{

    /**
     * Constructor
     */
    public SealedAuction(int auctionID)
    {
    
        super(auctionID);
        
    }
    
    /**
     * this method will provid the winning bid
     * 
     * @return winning bid
     */
    public Bid getWinningBid(int lotID)
    {
        
        if(!this.getOpen())
            return super.getWinningBid(lotID);
            
        System.out.println("Bids are sealed.");
        
        return new Bid();
        
    }
 
    /**
     * add a bid to requested lot.
     * 
     * @param num  the lot number for which to add bid.
     * @param name  the name of the bidder.
     * @param amount  the amount the bidder is bidding.
     * 
     * @return true if auction is open, false if auction is closed.
     */
    public boolean placeBid(int lotID, String name, double amount)
    {

        // if bidder already has bids remove them.
        this.getLotByID(lotID).withdrawBids(name);
        
        // add new bid and return success state.
        return super.placeBid(lotID, name, amount);
        
    }
    
    /**
     * Report information regarding the class
     */
    public void report()
    {
        
        super.report();
        
    }
}
