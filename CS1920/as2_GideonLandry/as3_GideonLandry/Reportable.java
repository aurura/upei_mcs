
/**
 * This interface, implemented by all classes in this project, adds a method witch will report information regarding the class.
 *
 * @author Gideon Landry
 * @version March 8, 2020
 */
public interface Reportable
{
    
    /**
     * Report information regarding the class
     */
    void report();

}
