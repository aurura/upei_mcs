
/**
 * This interface, implemented by the class Lot, adds a class that will be used to report winner information.
 *
 * @author Gideon Landry
 * @version March 8, 2020
 */
public interface Winnable
{
    
    /**
     * Report the winner information.
     */
    void reportWinner();
    
}
