
/**
 * class for auction bids
 *
 * @author Gideon Landry
 * @version March 8, 2020
 */
public class Bid implements Comparable, Reportable
{
    
    // the name of the bidder.
    private String bidder;
    
    // the amount the bidder bid.
    private double bid;

    /**
     * Default Constructor
     */
    public Bid()
    {

        bidder = "";
        bid = 0;
        
    }
    
    public Bid(String name, double value)
    {
        
        this.bidder = name;
        this.bid = value;
        
    }
    
    // setters
    public void setBidder(String bidder)
    {
        
        this.bidder = bidder;
        
    }
    
    public void setBid(double bid)
    {
        
        this.bid = bid;
        
    }
    
    // getters
    public String getBidder()
    {
        
        return bidder;
        
    }
    
    public double getBid()
    {
        
        return bid;
        
    }
    
    /**
     * Report information regarding the class
     */
    public void report()
    {
        
        System.out.println("Bidder: " + bidder + " Bid: " + bid);
        
    }

}
 