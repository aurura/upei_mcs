
/**
 * Abstract class Article - write a description of the class here
 *
 * @author Gideon Landry
 * @version February 4, 2020
 */
public abstract class Article
{

    // public utilities - will be directly accessable by all.
    public final String nl = System.lineSeparator();

    // Artical variables
    private String source;
    private String date;
    private String category;
    private String headline;
    private String body;

    // Article default constructer.
    public Article()
    {

        source = "";
        date = "";
        category = "";
        headline = "";
        body = "";

    }

    // Article custom constructer.
    public Article(String source, String date, String category)
    {

        this.source = source;
        this.date = date;
        this.category = category;
        this.headline = "";
        this.body = "";

    }

    //  mutator methods

    public void setSource(String source)
    {

        this.source = source;

    }

    public void setDate(String date)
    {

        this.date = date;

    }

    public void setCategory(String category)
    {

        this.category = category;

    }

    public void setHeadline(String headline)
    {

        this.headline = headline;

    }

    public void setBody(String body)
    {

        this.body = body;

    }

    //  accessor methods

    public String getSource()
    {

        return source;   

    }

    public String getDate()
    {

        return date;

    }

    public String getCategory()
    {

        return category;

    }

    public String getHeadline()
    {

        return headline;

    }

    public String getBody()
    {

        return body;

    }

    /**
     * This methode should print all metadata in an appropriat format.
     */
    public abstract void printMetadata();

    /**
     * this methode should print the article in an appropriat format.
     */
    public abstract void printStory();
}
