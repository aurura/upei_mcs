
/**
 * this is a class for weather news articles.
 *
 * @author Gideon Landry
 * @version February 6, 2020
 */
public class Weather extends Article
{

    private String region;
    private String[] conditions = new String[5];
    private int[] temps = new int[5];

    public Weather()
    {

        // sets Artical variables
        super();
        region = "";

    }

    public Weather(String source, String date, String category, String region)
    {

        // sets Artical variables
        super(source, date, category);
        this.region = region;

    }

    //  Setters

    /**
     * 
     * @param String  region that weather effects
     */
    public void setRegion(String region)
    {

        this.region = region;

    }

    /**
     * 
     * @param String[]  an array of weather conditions for five day forcast.
     */
    public void setConditions(String[] conditions)
    {

        for(int i = 0; i < conditions.length; i++)
            this.conditions[i] = conditions[i];

    }

    /**
     * 
     * @param int[]  an array of temperatures for five day forcast.
     */

    public void setTemps(int[] temps)
    {

        for(int i = 0; i < temps.length; i++)
            this.temps[i] = temps[i];

    }
    
    //  Getters

    /**
     * 
     */
    public String getRegion()
    {

        return region;

    }

        /**
     * 
     * @param conditions  an array of weather conditions for five day forcast.
     */
    public String[] getConditions()
    {

        return conditions;

    }

    /**
     * 
     * @return temps  an array of temperatures for five day forcast.
     */

    public int[] getTemps()
    {

        return temps;

    }
    
    /**
     * This methode should print all metadata in an appropriat format.
     */
    public void printMetadata()
    {
        
        System.out.println
        (
        "Source: " + super.getSource() + nl
        + "Date: " + super.getDate() + nl
        + "Category: "  + super.getCategory() + nl
        + "Region: " + region + nl
        );
        
    }

    /**
     * this methode should print the article in an appropriat format.
     */
    public void printStory()
    {
        System.out.println(super.getHeadline() + nl + "The weather in Region " + region + " for the next 5 days will be: " + nl);
        // display 5 day forcast.
        for(int i = 0; i < 5; i++)
            System.out.println(super.getDate() + (i + 1) + conditions[i] + ", " + temps[i] + "c" + nl);

    }

}
