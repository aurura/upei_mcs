
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * this is a class for world news articles.
 *
 * @author Gideon Landry
 * @version February 6, 2020
 */
public class International extends Article
{
    private String author;
    private String country;

    // default constructor.
    public International()
    {

        // sets Artical variables
        super();

        author = "";
        country = "";

    }

    public International(String source, String date, String category, String author, String country)
    {

        // sets Artical variables
        super(source, date, category);

        this.author = author;
        this.country = country;

    }

    //  Setters

    /**
     * 
     * @param String  author of artical.
     */
    public void setAuthorByline(String author)
    {

        this.author = author;

    }

    /**
     * 
     * @param String  artical origin country.
     */
    public void setCountry(String country)
    {

        this.country = country;

    }

    //  Getters

    /**
     * 
     */
    public String getAuthor()
    {

        return author;

    }

    /**
     * 
     */
    public String getCountry()
    {

        return country;

    }

    /**
     * This methode should print all metadata in an appropriat format.
     */
    public void printMetadata()
    {

        System.out.println
        (
            "Source: " + super.getSource() + nl
            + "Date: " + super.getDate() + nl
            + "Category: "  + super.getCategory() + nl
            + "Author: " + author + nl
        );

    }

    /**
     * this methode should print the article in an appropriat format.
     */
    public void printStory()
    {

        System.out.println
        (
            super.getHeadline() + nl + nl
            + super.getBody() + nl
        );

    }

}
