
/**
 * this is a class for news articles within Canada.
 *
 * @author Gideon Landry
 * @version February 6, 2020
 */
public class National extends Article
{

    private String author;
    private String province;

    public National()
    {

        // call Article class default constructor.
        super();

        author = "";
        province = "";

    }

    public National(String source, String date, String category, String author, String province)
    {

        // calls artical constructer
        super(source, date, category);

        this.author = author;
        this.province = province;

    }

    //  Setters

    /**
     * 
     * @param String  author of artical.
     */
    public void setAuthorByline(String author)
    {

        this.author = author;

    }

    /**
     * 
     * @param String  artical origin province within Canada.
     */
    public void setCountry(String province)
    {

        this.province = province;

    }

    //  Getters

    /**
     * 
     */
    public String getAuthor()
    {

        return author;

    }

    /**
     * 
     */
    public String getprovince()
    {

        return province;

    }

    /**
     * This methode should print all metadata in an appropriat format.
     */
    public void printMetadata()
    {

        System.out.println
        (
            "Source: " + super.getSource() + nl
            + "Date: " + super.getDate() + nl
            + "Category: "  + super.getCategory() + nl
            + "Author: " + author + nl
            + "Province: " + province + nl
            + "     " + super.getBody()
        );

    }

    /**
     * this methode should print the article in an appropriat format.
     */
    public void printStory()
    { 
                
        System.out.println
        (
            super.getHeadline() + nl + nl
            + super.getBody() + nl
        );
 
    }

}
