
/**
 * this is a class for sports news articles.
 *
 * @author Gideon Landry
 * @version February 6, 2020
 */
public class Sports extends Article
{

    private String author;
    private String sport;
    private String winner;
    private String losser;
    private int winScore;
    private int lossScore;

    public Sports()
    {

        // sets Artical variables
        super();
        author = "";
        sport = "";
        winner = "";
        losser = "";
        winScore = 0;
        lossScore = 0;

    }

    public Sports(String source, String date, String category, String author, String sport, String winner, String losser, int winScore, int lossScore)
    {

        // sets Artical variables
        super(source, date, category);

        this.author = author;
        this.sport = sport;
        this.winner = winner;
        this.losser = losser;
        this.winScore = winScore;
        this.lossScore = lossScore;

    }

    //  Setters

    /**
     * 
     * @param String  author of artical.
     */
    public void setAuthorByline(String author)
    {

        this.author = author;

    }

    /**
     * 
     * @param String  sport the artical refers to.
     */
    public void setSport(String sport)
    {

        this.sport = sport;

    }

    /**
     * 
     * @param String  the winning team or player.
     */
    public void setWinner(String winner)
    {

        this.winner = winner;

    }

    /**
     * 
     * @param String the lossing team or player.
     */
    public void setLosser(String losser)
    {

        this.losser = losser;

    }

    /**
     * 
     * @param int  winner score.
     */
    public void setWinScore(int winScore)
    {

        this.winScore = winScore;

    }

    /**
     * 
     * @param int losser score.
     */
    public void setLossScore(int lossScore)
    {

        this.lossScore = lossScore;

    }

    //  Getters

    /**
     *
     * @return author 
     */
    public String getAuthor()
    {

        return author;

    }

    /**
     * 
     * @return sport
     */
    public String getSport()
    {

        return sport;

    }

    /**
     * 
     * @return winner
     */
    public String getWinner()
    {

        return winner;

    }

    /**
     * 
     * @return losser
     */
    public String getLosser()
    {

        return losser;

    }

    /**
     * 
     * @return winScore
     */
    public int getWinScore()
    {

        return winScore;

    }

    /**
     * 
     * @return lossScore
     */
    public int getLossScore()
    {

        return lossScore;

    }

    /**
     * This methode should print all metadata in an appropriat format.
     */
    public void printMetadata()
    {

        System.out.println
        (
            "Source: " + super.getSource() + nl
            + "Date: " + super.getDate() + nl
            + "Category: "  + super.getCategory() + nl
            + "By: " + author + nl
            + "Sport: " + sport + nl
            + "Teams: " + winner + " vs. " + losser + nl
            + "Scores " + winScore + " - " + lossScore + nl
        );

    }

    /**
     * this methode should print the article in an appropriat format.
     */
    public void printStory()
    {

        System.out.println
        (
            super.getHeadline() + nl + nl
            + "“Opponents " + winner + " defeated " + losser + " with a score of " + winScore + " to " + lossScore + nl
            + super.getBody()
        );

    }

}
