
/**
 * this is a class for local news articles.
 *
 * @author Gideon Landry
 * @version February 6, 2020
 */
public class Local extends Article
{

    private String author;
    private String municipality;

    public Local()
    {

        // sets Artical variables
        super();

        author = "";
        municipality = "";

    }

    public Local(String source, String date, String category, String author, String municipality)
    {

        // sets Artical variables
        super(source, date, category);

        this.author = author;
        this.municipality = municipality;

    }

    //  Setters

    /**
     * 
     * @param String  author of artical.
     */
    public void setAuthorByline(String author)
    {

        this.author = author;

    }

    /**
     * 
     * @param String  artical origin municipality within province.
     */
    public void setMunicipality(String municipality)
    {

        this.municipality = municipality;

    }

    //  Getters

    /**
     * 
     */
    public String getAuthor()
    {

        return author;

    }

    /**
     * 
     */
    public String getMunicipality()
    {

        return municipality;

    }

    /**
     * This methode should print all metadata in an appropriat format.
     */
    public void printMetadata()
    {

        System.out.println
        (
            "Source: " + super.getSource() + nl
            + "Date: " + super.getDate() + nl
            + "Category: "  + super.getCategory() + nl
            + "Author: " + author + nl
        );

    }

    /**
     * this methode should print the article in an appropriat format.
     */
    public void printStory()
    {

        System.out.println
        (
            super.getHeadline() + nl + nl
            + super.getBody() + nl
        );

    }

}
