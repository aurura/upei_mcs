import java.util.ArrayList;

/**
 * this app will create and test the Article class and all its subclasses.
 *
 * @author Gideon Landry
 * @version February 6, 2020
 */
public class NewsFeedTester
{
    public static void main (String[] args)
    {
        // this will hold all articles of any type.
        ArrayList<Article> articles = new ArrayList<Article>();

        // create news articals
        
        // Source url: https://www.theglobeandmail.com/world/article-outrage-after-gunman-ambushes-nyc-police-twice-in-12-hours/
        International international = new International("The Globe and Mail", "February 9, 2020", "SOPHIA ROSENBAUM AND DEEPTI HAJELA", "International", "USA");
        international.setHeadline("Outrage after gunman ambushes NYC police twice in 12 hours");
        international.setBody("A gunman was arrested Sunday after he ambushed police officers in the Bronx twice in 12 hours, wounding two in attacks that brought outrage from officials who blamed the violence on an atmosphere of anti-police rhetoric, authorities said.");

        // Source url: https://globalnews.ca/news/6528250/coronavirus-quarantine-canadian-armed-forces-medics/
        National national1 = new National("Global News", "February 9, 2020", "National", "KERRI BREEN", "Ontario");
        national1.setHeadline("Canadian Armed Forces medics on Wuhan flight permitted to leave coronavirus quarantine");
        national1.setBody("Armed Forces medical staff who accompanied a group of Canadians on a flight home from Wuhan, China, have been given permission to leave quarantine early.");

        // Source url: https://calgaryherald.com/news/politics/braid-in-public-shift-kenney-says-alberta-has-to-go-green-over-time
        National national2 = new National("Calgary Herald", "February 10, 2020", "National", "DON BRAID", "Alberta");
        national2.setHeadline("Braid: In public shift, Kenney says Alberta has to go green over time");
        national2.setBody("Suddenly, Premier Jason Kenney is talking publicly about the “energy transition.”");

        // Source url: https://montreal.ctvnews.ca/more-snow-on-the-way-as-montreal-officials-hasten-to-clear-the-streets-1.4804796
        National national3 = new National("CTV", "February 10, 2020", "National", "Rachel Lau", "Québec");
        national3.setHeadline("More snow on the way as Montreal officials hasten to clear the streets");
        national3.setBody("MONTREAL -- Get your shovels out – there's more snow on the way for, well, most of this week.");

        // Source url: https://www.journalpioneer.com/lifestyles/local-lifestyles/stage-door-charlottetown-jazz-ensemble-taking-road-trip-feb-15-408370/
        Local local = new Local("Journal Pioneer", "February 7, 2020", "Local", "Sally Cole", "Charlottetown");
        local.setHeadline("STAGE DOOR: Charlottetown Jazz Ensemble taking road trip Feb. 15");
        local.setBody("The Charlottetown Jazz Ensemble is breaking away from tradition.");

        // Source url: https://www.hothothoops.com/2020/2/9/21131000/heat-lose-third-straight-fall-to-blazers-hassan-whiteside-bam-adebayo-goran-dragic-damian-lillard
        Sports sport = new Sports("source", "February 9,2020", "Sports", "Brandon Di Perno", "Basketball", "Miami", "Portland", 115, 109);
        sport.setHeadline("Heat lose third straight, fall to Blazers 115-109");
        sport.setBody("The Heat struggled to score early on going scoreless in the first two minutes. Trevor Ariza stepped up for Portland scoring their first eight points. Duncan Robinson would end Miami’s drought with two threes, but Jimmy’s absence was felt early on. For Portland, CJ McCollum and Carmelo Anthony both punished the Heat and frequently enough for Spo to go the reserves early.");

        //Source url:  https://weather.com/weather/today/l/46.25,-63.15?par=google&temp=c
        Weather weather = new Weather("The Weather Channel", "February 10, 2020", "Weather", "Charlottetown, PE");
        weather.setHeadline("Charlottetown 5 Day Forcast");
        int[] temps = {-11, -2, -5, -13, -18};
        String[] conditions = {"Snow","Cloudy","Snow","Snow","Cloudy"};
        weather.setConditions(conditions);
        weather.setTemps(temps);
        
        // add all articals to arraylist
        articles.add(international);
        articles.add(national1);
        articles.add(national2);
        articles.add(national3);
        articles.add(local);
        articles.add(sport);
        articles.add(weather);

        // print all articles of cateogory 'National'.
        printCategory("National", articles);
        
        // print all articles dated 'February 9, 2020'.
        printDate("February 9, 2020", articles);
        
        printAll(articles);
    }

    /**
     * Prints all articles.
     * 
     * @param Arraylist  list of all articles.
     */
    public static void printAll(ArrayList<Article> articles)
    {

        for(Article article : articles)
        {
            // print article
            System.out.println(article.nl);
            article.printMetadata();
            article.printStory();

        }

    }

    /**
     * Prints all articles of the given type 
     * 
     * @param String  the category of articles to look for.
     * @param Arraylist  a list of all articles
     */
    public static void printCategory(String category, ArrayList<Article> articles)
    {

        for(Article article : articles)
        {
            // if category of artical matches search category print artical.
            if(article.getCategory().equals(category))
            {
                // print article
                System.out.println(article.nl);
                article.printMetadata();
                article.printStory();
            }

        }

    }

    /**
     * Prints all articles from the date given.
     * 
     * @param String  the date of the articles to display.
     * @param Arraylist  list of all articles.
     */
    public static void printDate(String date, ArrayList<Article> articles)
    {

        for(Article article : articles)
        {
            // if date matches artical date, print artical
            if(article.getDate().equals(date))
            {
                // print article
                System.out.println(article.nl);
                article.printMetadata();
                article.printStory();
            }

        }

    }
}
