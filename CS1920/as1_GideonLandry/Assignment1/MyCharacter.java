
/**
 * This class is for creating characters for a fantasy/adventure game.
 *
 * @author Gideon Landry
 * @version January 24, 2020
 */
public class MyCharacter
{
    // utilities.
    public static final String ls = System.lineSeparator();

    // class attributes.
    private String name;
    private String characterClass;

    private int health;
    private int mana;
    private int strength;

    private double equipLoad;

    /**
     * default constructor.
     */
    public MyCharacter()
    {

        name = "";
        characterClass = "";
        health = 0;
        mana = 0;
        strength = 0;
        equipLoad = 0;

    }

    /**
     * constructor that takes in all attribute values and applies them to object.
     * 
     * @param String name
     * @param String characterClass
     * @param int health
     * @param int mana
     * @param int strength
     * @param double equipLoad
     */
    public MyCharacter(String name, String characterClass, int health, int mana, int strength, double equipLoad)
    {

        this.name = name;
        this.characterClass = characterClass;
        this.health = health;
        this.mana = mana;
        this.strength = strength;
        this.equipLoad = equipLoad;

    }

    // Below are mutator(or setter) methods.

    /**
     * 
     * @param name
     */
    public void setName(String name)
    {

        this.name = name;

    }

    /**
     * 
     * @param health
     */
    public void setHealth(int health)
    {
        // 'this.health' is the class attribute 'health' is the input methode parameter.
        this.health = health;

    }

    /**
     * 
     * @param characterClass
     */

    public void setCharacterClass(String characterClass)
    {

        this.characterClass = characterClass;

    }

    /**
     * 
     * @param strength
     */
    public void setStrength(int strength)
    {

        this.strength = strength;

    }

    /**
     * 
     * @param mana
     */
    public void setMana(int mana)
    {

        this.mana = mana;

    }

    /**
     * 
     * @param equipLoad
     */
    public void setEquipLoad(double equipLoad)
    {

        this.equipLoad = equipLoad;

    }

    // Below are accessor(or getter) methods.

    /**
     * 
     * @return name of character.
     */
    public String getName()
    {

        return name;

    }

    /**
     * 
     * @return health capacity of character.
     */
    public int getHealth()
    {

        return health;

    }

    /**
     * 
     * @return characterClass
     */
    public String getCharacterClass()
    {

        return characterClass;

    }

    /**
     * 
     * @return strength of character
     */
    public int getStrength()
    {

        return strength;

    }

    /**
     * 
     * @return mana capacity of character
     */
    public int getMana()
    {

        return mana;

    }

    /**
     * 
     * @return equipLoad  or carrying capacity of character.
     */
    public double getEquipLoad()
    {

        return equipLoad;

    }

    /**
     * this methode will provide a description of the attributes for MyCharacter.
     * 
     */
    public static void describe()
    {
        
        // print descriptions of attributes and setting.
        System.out.println
        (
            "Fantasy adventure. Venturing into the open world, fighting monstors," + ls +
            "exploring dungeons, and robbing other travellers." + ls + ls +
            "Name: the name of the character." + ls +
            "Character Class: The class a character specialises in such as a \"Mage\", \"Warrior\", or \"Thief\"." + ls +
            "Health: The amount of health the character has. (20-100)" + ls +
            "Mana: The amount of mana one has to spend on magical abilities. (0-40)" + ls +
            "Strength: The amount of strength one has to inflict damage using a melee weapon. (5-40)" + ls +
            "Equipment Load: The carrying capacity of the character. (20.0-30.0)" + ls
        );

    }

    /**
     * this methode will print attribute values.
     * 
     */
    public void printAttributes()
    {

        System.out.println
        (

            "Name: " + name + ls +
            "Character Class: " + characterClass + ls +
            "Health: " + health + ls +
            "Mana: " + mana + ls +
            "Strength: " + strength + ls +
            "Equipment Load: " + equipLoad + ls

        );

    }

    /**
     * this method overrides default toString method to produce a string containing character 
     * narrative with attribute values. 
     *
     * @return String  a narrative of the character.
     */
    @Override
    public String toString()
    {
        
        // return character narrative as a string.
        return name + " starts their jurney as a " + characterClass + " with " + health + " units of health, " + ls +
        mana + " units of mana for casting magic, " + strength + " units of strength for swinging a sword," + ls +
        "and a carrying capacity of " + equipLoad + " for gear."; 
        
    }
}
