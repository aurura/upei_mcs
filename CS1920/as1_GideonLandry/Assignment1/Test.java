
/**
 * main class to test MyCharacter class objects.
 *
 * @author Gideon Landry
 * @version January 24, 2020
 */
public class Test
{
    public static final String ls = System.lineSeparator();

    public static void main (String[] args)
    {

        // call static describe method to provide context and info on expected attributes.
        MyCharacter.describe();

        // creating character using default constructor
        MyCharacter defChara = new MyCharacter();

        // create test character objects with attributes.
        MyCharacter testChara1 = new MyCharacter("Michael", "mage", 40, 50, 5, 25.0);
        MyCharacter testChara2 = new MyCharacter("John", "warrior", 100, 0, 40, 28.8);
        
        // display all default character stats using getters.
        System.out.println
        (

            "Name: " + defChara.getName() + ls +
            "Character Class: " + defChara.getCharacterClass() + ls +
            "Health: " + defChara.getHealth() + ls +
            "Mana: " + defChara.getMana() + ls +
            "Strength: " + defChara.getStrength() + ls +
            "Equipment Load: " + defChara.getEquipLoad() + ls

        );
        
        // display default character narrative, calles toString method.
        System.out.println(defChara + ls);

        // update all default character stats using setters.
        defChara.setName("Joel");
        defChara.setCharacterClass("thief");
        defChara.setHealth(50);
        defChara.setMana(20);
        defChara.setStrength(20);
        defChara.setEquipLoad(30.0);

        // print info for default character
        defChara.printAttributes();
        System.out.println(defChara + ls);

        // print info for first test character
        testChara1.printAttributes();
        System.out.println(testChara1 + ls);

        // print info for second test character
        testChara2.printAttributes();
        System.out.println(testChara2 + ls);

    }
}
