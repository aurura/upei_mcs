/*
 * This app will simulate a race between the tortoise and the hare.
 * Author: Gideon Landry
 * Student ID: 0144269
 * Created: December 1, 2020
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void moveTortoise(int *pos);
void moveHare(int *pos);
void printCurrentPositions(const int harePos, const int tortoPos, const int dis);

int main(void)
{

    const int dis = 70;

    int hare = 0;
    int tortoise = 0;

    srand(time(NULL));

    printf("%s","BANG!!!!!\nAND THEY'RE OFF!!!!!\n");

    // Start race.
    while(hare < dis && tortoise < dis)
    {
        // pass tortoise and hare positions by memory address to their respective move functions.
        moveTortoise(&tortoise);
        moveHare(&hare);

        // print the position of the tortoise and hare.
        printCurrentPositions(hare, tortoise, dis);
    }

    // declare winner.
    if(hare >= dis && tortoise >= dis)
    {
        printf("%s\n", "It's a tie.");
    }
    else if(hare >= dis)
    {
        printf("%s\n", "Hare wins");
    }
    else
    {
        printf("%s\n", "TORTOISE WINS!!! YAY!!!");
    }
}

/*
 * Move the tortoise's position.
 *
 * Takes int pointer as paramater.
 */
void moveTortoise(int *pos)
{

    int ranNum = 1 + rand() % 10;

    // Fast plod
    if(ranNum <= 5)
    {
        *pos += 3;
    }
    // Slip
    else if(ranNum <= 7)
    {
        if((*pos - 6) < 1)
        {
            *pos = 1;
            return;
        }
        *pos -= 6;
    }
    // Slow plod
    else
    {
        *pos += 1;
    }
}

/*
 * Move the hare's position.
 *
 * Takes int pointer as paramater
 */
void moveHare(int *pos)
{

    int ranNum = 1 + rand() % 10;

    // Big hop
    if(ranNum <= 2)
    {
        *pos += 9;
    }
    // Big slip
    else if(ranNum == 3)
    {
        if((*pos - 12) < 1)
        {
            *pos = 1;
            return;
        }
        *pos -= 12;
    }
    // Small hop
    else if(ranNum <= 6)
    {
        *pos += 1;
    }
    // Small slip
    else if(ranNum <= 8)
    {
        if((*pos - 2) < 1)
        {
	    *pos = 1;
	    return;
        }
        *pos -= 2;
    }
}

// display the current position of the tortoise and the hare.
void printCurrentPositions(const int harePos, const int tortoPos, const int dis)
{

    for(size_t i = 1; i <= dis; ++i)
    {
        if(i == harePos || i == tortoPos)
        {
            // If the hare and tortoise are in same space the tortoise will bite hare.
            if(harePos == tortoPos)
            {
                printf("%s","OUCH!!!!");
            }
            else if(i == harePos)
            {
                printf("%s","H");
            }
            else
            {
                printf("%s","T");
            }
        }
        else
        {
            printf("%s","-");
        }
    }
    printf("\n");
}
