// Move mechanical turtle around floor and draw patterns.
// Author: Gideon Landry
// Student ID: 0144269
// Created: November 24, 2020
#include <stdio.h>
#define SIZE 100
#define COL 50
#define ROW 50

// prototypes
void drawTurtlePath(int path[ROW][COL],int dis);
void printFloor(int path[ROW][COL]);
void collectCommands(int commands[SIZE]);

int mode, row, col, pen = 0;

int main(void)
{

    int commands[SIZE];
    int path[ROW][COL];

    for(size_t i = 0; i < ROW; ++i)
    {
        for(size_t j = 0; j < COL; ++j)
        {
	    path[i][j] = 0;
	}
        commands[i] = i;
    }

    // get commands from user.
    collectCommands(commands);

    for(size_t i = 0; i < SIZE; ++i)
    {
        switch (commands[i])
        {
            case 1: // pen up
                pen = 0;
                break;
            case 2: // pen down
                pen = 1;
                break;
            case 3: // turn right
                if(mode >= 3)
                {
                    mode = 0;
                }
                else
                {
                    mode = mode + 1;
                }
                break;
            case 4: // turn left
                if(mode <= 0)
                {
                    mode = 3;
                }
                else
                {
                    mode = mode - 1;
                }
                break;
            case 5: // move forward.
                i = i + 1; // next position in array will hold distance to travel
                drawTurtlePath(path,commands[i]);
                break;
            case 6: // print the floor
                printFloor(path);
                break;
            case 9: // end
                i = SIZE;
                break;
        }
    }
}

// This function will move turtle and draw path if pen is down.
void drawTurtlePath(int path[ROW][COL], int dis)
{
    for(size_t i = dis; i > 0; --i)
    {
        switch (mode)
        {
            case 0: // move right
                if(col >= (COL - 1))
                {
                    col = COL - 1;
                    return;
                }
                col = col + 1;
                break;
            case 1: // move down
	        if(row >= (ROW - 1))
	        {
	            row = ROW - 1;
	            return;
	        }
	        row = row + 1;
                break;
            case 2: // move left
	        if(col <= 0)
	        {
	            col = 0;
	            return;
	        }
	        col = col - 1;
                break;
            case 3: // move up
                if(row <= 0)
                {
                    row = 0;
                    return;
                }
                row = row - 1;
	        break;
        }

        // if pen is down leave mark
        if(pen == 1)
        {
            path[row][col] = 1;
        }
    }
}

// This function will print floor
void printFloor(int path[ROW][COL])
{
    for(size_t i = 0; i < ROW; ++i)
    {
        for(size_t j = 0; j < COL; ++j)
	{
	    if(path[i][j] == 1)
	    {
		 printf("*");
	    }
	    else
	    {
		printf(" ");
	    }
	}
	printf("\n");
    }
}

// This function will request and store commands.
void collectCommands(int commands[SIZE])
{

    int input;

    for(size_t i = 0; i < SIZE; ++i)
    {
        printf("%s","Enter command:\n");
        scanf("%d", &input);
	commands[i] = input;
        if(input == 9)
	{
	    i = SIZE;
   	}
	if(input == 5)
        {
	    i = i + 1;
	   // printf("%s","Distance:\n");
	    scanf(",%d", &input); // get distance to travel from user
    	    commands[i] = input;
        }
    }
}
