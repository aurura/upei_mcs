// CS-1620-1 Assignment 3 Q2
// author: Gideon Landry
// Date: March 25, 2020

module test ();

	reg A,B,C,D,E;
	output [2:1] F;
	integer count;
	
	Q2b q2b (A, B, C, D, E, F);

	initial begin
			$display("Test: Q2a");
			$display("ABCDE |  F1 F2");
			$display("===== |  == ==");
			for (count = 0; count < 32; count = count + 1) begin // verilog has no ++ operator
				{A, B, C, D, E} = count[4:0]; // rightmost bits of count are the three bits we need
				#10 // wait 10 ticks
				$display("%5b |  %b  %b", ({A, B, C, D, E}), F[1], F[2]);
			end
		end

endmodule

module Q2a (A, B, C, D, E, F);

	input A,B,C,D,E;
	output [1:0] F;
	wire t1, t2, t3, t3_not, f1, f2;
	
	and AND (t1, A, B);
	nor NOR (t2, C, D);
	xor XOR (t3, t1, t2);
	not t3_inv (t3_not, t3);
	nand NAND (f2, t2, E);
	xnor XNOR (f1, t3_not, f2);
	
	assign F[0] = f1;
	assign F[1] = f2;
	
endmodule

module Q2b (A, B, C, D, E, F);

	input A,B,C,D,E;
	output [1:0] F;
	
	assign F[0] = ~(A & B) | ~E & ~((A & B) ^ ~(C | D));
	assign F[1] = ~E | ~(C + D);

endmodule