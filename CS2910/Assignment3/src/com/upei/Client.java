//package com.upei;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

import static java.lang.System.out;

/**
 *
 * @author Gideon Landry
 * @since December 2, 2021
 */
public class Client {


    public static void main(String[] args) throws Exception {

        final int SERVER_PORT = 12345;
        final int BUF_SIZE = 4096;

        byte[] bytes = new byte[BUF_SIZE];

        // create socket with server name and server port.
        Socket s = new Socket(args[0],SERVER_PORT);

        // create buffer with BUF_SIZE as limit.
        DataInputStream buf = new DataInputStream(new BufferedInputStream(s.getInputStream()));

        // the output to send to server. sends file name.
        PrintStream c = new PrintStream(s.getOutputStream());

        c.print(args[1]);

        int b;
        while ((b = buf.read(bytes)) > 0) {

            // write to file.
            out.write(bytes,0,b);

        }

        buf.close();
        c.close();

    }
}
