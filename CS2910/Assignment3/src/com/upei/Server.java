//package com.upei;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Server {
    public static void main(String[] args) throws IOException {

        final int SERVER_PORT = 12345;
        final int BUF_SIZE = 4096;
        final int QUEUE_SIZE = 10;

        byte[] bytes = new byte[BUF_SIZE];

        // create the socket for the server.
        ServerSocket s = new ServerSocket(SERVER_PORT,QUEUE_SIZE);

        while(true) {


            // create socket for client.
            Socket sa = s.accept();

            // create buffer for input stream.
            InputStream inputStream = sa.getInputStream();

            BufferedInputStream bufIn = new BufferedInputStream(inputStream);

            String fileName = new String(bufIn.readAllBytes(), StandardCharsets.UTF_8);

            System.out.println(fileName);

            File file = new File(fileName);

            // the output to send to client.
            FileInputStream c = new FileInputStream(file);

            // create file stream to output to client.
            BufferedInputStream buf = new BufferedInputStream(c);

            //buf.read(bytes,0,bytes.length);

            OutputStream output = sa.getOutputStream();

            int b;
            while ((b = buf.read(bytes)) > 0) {
               output.write(bytes,0,b);
            }

            sa.close();
            buf.close();
            c.close();

        }
    }
}
