package com.upei;

public class Channel {
    public short sin_family;
    public short sin_port;
    public long s_addr;
    public char[] sin_zero = new char[8];
}
