package diamant;

public class TimidPlayer extends diamant.Player {

    public TimidPlayer(diamant.Pawn playerColour, int playerNumber)
    {
        super(playerColour, playerNumber);
    }

    /** A timid player will leave a mine as soon as one trap appears.
     * @param numTraps - number of traps revealed
     * @param numRubies - number of rubies in the mine
     * @param numPlayers - number of players in the mine
     * @param cardsRemain - number of cards left ot be revealed in the deck
     * @return
     */
    public boolean leaveMine(int numTraps, int numRubies, int numPlayers, int cardsRemain)
    {
        if (numTraps>=1)
        {
            this.leave = true;
            this.inMine = false;
            return true;

        }
        this.leave = false;
        this.inMine = true;

        return false;
    }

    public String reportStrategy()
    {
        return "Timid";
    }

    /**
     * Test suite for TimidPlayer.
     *
     * created by Gideon Landry
     * October 28, 2021
     * @param args
     */
    public static void main(String[] args)
    {
        TimidPlayer timidPlayer = new TimidPlayer(Pawn.RED,1);

        // Gideon: test report strategy method. should return "Timid"
        if(timidPlayer.reportStrategy().equals("Timid"))
            System.out.println("Test: reportStrategy SUCCESS");
        else
            System.out.println("Test: reportStrategy FAIL");

        // Gideon: test that the player won't leave if there are no traps revealed.
        if(!timidPlayer.leaveMine(0, 10, 6, 15))
            System.out.println("Test: leaveMine1 SUCCESS");
        else
            System.out.println("Test: leaveMine1 FAIL");

        // Gideon: test that the player will leave if there are 1 or more traps revealed.
        if(timidPlayer.leaveMine(2, 10, 6, 15))
            System.out.println("Test: leaveMine2 SUCCESS");
        else
            System.out.println("Test: leaveMine2 FAIL");

    }
}
