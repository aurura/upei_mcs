package diamant;

import java.util.Random;

public class BalancePlayer extends diamant.Player {

    public BalancePlayer(diamant.Pawn playerColour, int playerNumber)
    {
        super(playerColour, playerNumber);
    }

    /** A balanced player will stay in the mine until 2 trap cards have been revealed and then have a 50/50 chance of leaving.
     * @param numTraps - number of traps revealed
     * @param numRubies - number of rubies in the mine
     * @param numPlayers - number of players in the mine
     * @param cardsRemain - number of cards left ot be revealed in the deck
     * @return return true if leaving the mine, false otherwise.
     */
    public boolean leaveMine(int numTraps, int numRubies, int numPlayers, int cardsRemain)
    {
        Random rand = new Random();

        if(numTraps >=2)
        {
            int chance = rand.nextInt(100);
            if(chance >= 51) {
                this.leave = true;
                this.inMine = false;
                return true;
            }
        }
        this.leave = false;
        this.inMine = true;

        return false;
    }

    public String reportStrategy()
    {
        return "Balance";
    }

    /**
     * Test suite for BalancePlayer.
     *
     * created by Gideon Landry
     * October 28, 2021
     * @param args
     */
    public static void main(String[] args)
    {
        BalancePlayer balancePlayer = new BalancePlayer(Pawn.RED,1);

        // Gideon: test report strategy method. should return "Balance"
        if(balancePlayer.reportStrategy().equals("Balance"))
            System.out.println("Test: reportStrategy SUCCESS");
        else
            System.out.println("Test: reportStrategy FAIL");

        // Gideon: test that the player will not leaving if there are less than 2 traps revealed. (manual check)
        System.out.println("Test: leaveMine1 (Manual check). Should always be false.");
        for(int i = 0; i < 20; i++)
            System.out.println(balancePlayer.leaveMine(1,10,6,20) + "");

        // Gideon: test that the player has about a 50% chance of leaving if there are 2 or more traps revealed. (manual check)
        System.out.println("\n\rTest: leaveMine2 (Manual check)");
        for(int i = 0; i < 20; i++)
            System.out.println(balancePlayer.leaveMine(2,10,6,20) + "");


    }
}
