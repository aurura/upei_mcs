package diamant;

/**
 * Modified by Gideon Landry
 * October 27, 2021
 */
public class MineTile{

    // indicator if a card is face up (true) or face down (false)
    private boolean up = false;

    // the value of the tile; positive integer for treasure, negative 1 for traps
    private int tileValue = 0;

    // indicator if a card is a trap card; false if not a trap, true if there is a trap
    private boolean trap = false;

    /** Create a mine tile with the value passed through the first parameter. Tiles default to not being traps and being face down.
     *
     * @param value - the value of the card
     */
    public MineTile(int value)
    {
        /* TODO */
        // Gideon: set default values for this tile.
        tileValue = value;
        if(value == 0)
            trap = true;
    }

    /** Retrieve the value of the tile.
     *
     * @return the value of the card
     */
    public int getTileValue() {
        /* TODO */
        // Gideon: return tile value.
        return tileValue;
    }

    /** Test if there is a trap on a tile.
     *
     * @return true if the tile is a trap, false if it is not a trap.
     */
    public boolean isTrap() {
        /* TODO */
        // Gideon: return value of trap.
        return trap;
    }

    /** Set a tile to be a trap or not.
     *
     * @param trap true if the tile is to have a trap, false otherwise.
     */
    public void setTrap(boolean trap) {
        /* TODO */
        // Gideon: set instance trap to value of parameter trap.
        this.trap = trap;
    }

    /** Turn a tile face up if it is down, and down if it is up.
     *
     * @return resulting orientation of the tile.
     */
    public boolean flipTile()
    {
        /* TODO */
        // Gideon: if tile is up flip down, otherwise flip up.
        if(up)
            turnFaceDown();
        else
            turnFaceUp();
        return up;
    }

    /** Test to see if a tile is face up.
     *
     * @return true if face up, false otherwise.
     */
    public boolean isFaceUp()
    {
        /* TODO */
        // Gideon return up value.
        return up;
    }

    /** Test to see if a tile is face down.
     *
     * @return true if face down, false otherwise.
     */
    public boolean isFaceDown()
    {
        /* TODO */
        // Gideon: returns opposite up value.
        return !up;
    }

    /** Turn a tile face down if it is up */
    public void turnFaceDown()
    {
        /* TODO */
        // Gideon: turn tile face down.
        up = false;
    }

    /** Turn a tile face up if it is down */
    public void turnFaceUp()
    {
        /* TODO */
        // Gideon: turn tile face up.
        up = true;
    }

    /** Convert a tile to a string for printing.
     * @return An asterisk * if face down, or its value if it is face up.
     * */
    public String toString()
    {
        /* TODO */
        // Gideon: return * if tile is face down, otherwise return tile value.
        if(isFaceDown()) {
            return "*";
        }

        return getTileValue() + "";
    }

    public static void main(String[] args)
    {
        MineTile three = new MineTile(3);

        three.turnFaceUp();

        if((three.toString().equals("3"))&&(three.isFaceUp()))
        {
            System.out.println("Test: MineTile turnFaceUp SUCCESS");
        }
        else
        {
            System.out.println("Test: MineTile turnFaceUp FAIL");
        }

        three.turnFaceDown();

        if((three.toString().equals("*"))&&(three.isFaceDown()))
        {
            System.out.println("Test: MineTile turnFaceDown SUCCESS");
        }
        else
        {
            System.out.println("Test: MineTile turnFaceDown FAIL");
        }

        three.flipTile();
        if((three.toString().equals("3"))&&(three.isFaceUp()))
        {
            System.out.println("Test: MineTile flipTile SUCCESS");
        }
        else
        {
            System.out.println("Test: MineTile flipTile FAIL");
        }


    }

}
