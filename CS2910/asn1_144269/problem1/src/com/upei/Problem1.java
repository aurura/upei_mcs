package com.upei;

/**
 * This program will simulate the Monty Hall Problem
 *
 * @author Gideon Landry
 * @since September 29, 2021
 *
 */
public class Problem1 {

    public static void main(String[] args) {

        // store the number of times the stay strategy is used and the number of times it results in a win.
        double numOfStays = 0;
        double numOfStayWins = 0;

        // store the number of times the swap strategy is used, and it's wins.
        double numOfSwaps = 0;
        double numOfSwapWins = 0;

        // run for 10000 trials
        for(int i = 0; i < 10000; i++) {

            // randomly places car behind door 1, 2, or 3.
            int carPlacement = (int) (Math.random() * 3) + 1;

            // randomly generate what door the player chooses.
            int playerChoice = (int) (Math.random() * 3) + 1;

            // door to be revealed.
            int revealDoor = 0;

            // if players choice is same as the car placement randomly choose what remaining door is revealed.
            if(carPlacement == playerChoice) {

                // randomly generate 1 or 2;
                int skip = (int) (Math.random() * 2) + 1;
                for(int j = 1; j < 4; j++)
                    if(j != carPlacement) {
                        // if skip is 1 choose other door that isn't players choice.
                        if(skip == 1) {
                            j++;
                            if (j == carPlacement)
                                j++;
                        }
                        revealDoor = j;
                    }
            }

            // if player choice is not same as car placement reveal door that is not car or player choice.
            else
               for(int j = 1; j < 4; j++)
                   if(j != carPlacement && j != playerChoice)
                       revealDoor = j;

            // decide whether to swap (1) or stay (2).
            int swap = (int) (Math.random() * 2) + 1;
            switch (swap) {
                case 1:
                    // change playerChoice to other unopened door.
                    for(int j = 1; j < 4; j++)
                        if(j != playerChoice && j != revealDoor)
                            playerChoice = j;

                    numOfSwaps++;
                    if (playerChoice == carPlacement)
                        numOfSwapWins++;
                    break;
                case 2:
                    numOfStays++;
                    if (playerChoice == carPlacement)
                        numOfStayWins++;
                    break;
            }
        }

        // display results.
        System.out.println(
                "Stay Counter: " + numOfStays + "\r\n"
                + "Stay Win Percentage: " + (numOfStayWins / numOfStays * 100) + "\r\n"
                + "Swap Counter: " + numOfSwaps + "\r\n"
                + "Swap Win Percentage: " + (numOfSwapWins / numOfSwaps * 100)
                );


    }
}
