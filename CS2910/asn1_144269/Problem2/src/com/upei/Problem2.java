package com.upei;

import java.util.Scanner;

/**
 * This program simulates JavaDice, a betting game using dice.
 *
 * @author Gideon Landry
 * @since September 29, 2021
 *
 */
public class Problem2 {

    // this array will hold the value of every dice: 0 & 1 are player's, 2 & 3 are opponents.
    public static int[] dice = new int[4];

    public static void main(String[] args) {

        double playerWallet = 20.00;
        int betValue = 0;
        String playerName;

        Scanner input = new Scanner(System.in);

        System.out.print("Please enter your name> ");
        playerName = input.nextLine();
        System.out.println("Welcome to JavaDice, " + playerName + ". You have $" + playerWallet + " in your wallet.");

        do {

            // get bet.
            System.out.print("How much would you like to bet (enter negative value to exit)? ");
            betValue = input.nextInt();

            // check if bet is greater than wallet amount.
            if(betValue > playerWallet)
               System.out.println("Your bet can't exceeed the amount in your wallet.");

            else if(betValue > 0) {

                // start round.
                playerWallet -= betValue;
                double winnings = playRound(betValue);
                playerWallet += winnings;

                // display dice results.
                System.out.printf("%s %20s\r\n","YOU ROLLED","COMPUTER ROLLED");
                System.out.printf("%4s-%s %15s-%s\r\n\r\n", dice[0], dice[1], dice[2],dice[3]);

                //declare win or loss
                if (winnings > 0)
                    System.out.println("You win!!!!");
                else
                    System.out.println("Sorry, you lost.");

                System.out.println("Your wallet now contains $" + playerWallet);
            }
            System.out.println("----------------------------------------------------------------------");

            // end if players bet is negative or if wallet reaches 0.
        } while (betValue >= 0 && playerWallet > 0);
        System.out.println("Thanks for playing " + playerName + "! Your wallet contains $" + playerWallet + ".");
    }

    /**
     * this methode will take in the players bet and apply JavaDice logic to generate winnings.
     *
     * @param playerBet the amount the player has bet.
     * @return the amount the player has won.
     */
    public static double playRound(double playerBet) {

        double winnings = 0;

        // throw dice
        for(int i = 0; i < dice.length; i++)
            dice[i] = roll();

        // if player dice are dubs and opponent dice are not, player wins.
        if((dice[0] == dice[1] && dice[2] != dice[3]))
            winnings = playerBet * 2.0;

        else {
            // if opponent dice are dubs and players dice are not then player losses.
            if (dice[0] != dice[1] && dice[2] == dice[3])
            {
                return winnings;
            }
            // if player rolled a larger value then opponent player wins.
            if((dice[0] + dice[1]) > (dice[2] + dice[3]))
                winnings = playerBet * 2.0;
        }

        return winnings;
    }

    /**
     * This method generates random number between 1 and 6.
     * @return a random value from 1 to 6.
     */
    public static int roll() {
        return (int) (Math.random() * 6) + 1;
    }
}
