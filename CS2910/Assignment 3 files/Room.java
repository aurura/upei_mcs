
import java.util.ArrayList;
import java.util.Iterator;

/**
 * This will hold room information for the maze.
 *
 * @author Gideon Landry
 * @since November 21, 2021
 */
public class Room implements MazeRoom {

    private String name;
    private String key = "";
    private ArrayList<String> gates = new ArrayList<>();
    private ArrayList<String> keys = new ArrayList<>();
    private ArrayList<String> rooms = new ArrayList<>();

    /**
     * @param
     */
    public Room(String name) {
        this.name = name;
    }

    /**
     *
     * @param key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     *
     * @param gate
     * @param key
     * @param room
     */
    public void setGate(String gate, String key, String room) {
        gates.add(gate);
        keys.add(key);
        rooms.add(room);
    }

    /**
     *
     * @return the name of the room.
     */
    @Override
    public String name() {
        return name;
    }

    /**
     *
     * @return the key held in this room.
     */
    @Override
    public String roomKey() {
        return key;
    }

    /**
     *
     * @return the MazeRoomIterator for this room.
     */
    @Override
    public Iterator<Action> iterator() {
        return new MazeRoomIterator(this).next();
    }

    /**
     * This class will create actions for each gate of room.
     *
     * @author Gideon Landry
     */
    public class MazeRoomIterator implements Iterator {

        Room room;
        ArrayList<Action> actions = new ArrayList<>();

        public MazeRoomIterator() {

        }

        public MazeRoomIterator(Room room) {

            this.room = room;

            // create actions.
            for(int i = 0; i < room.gates.size(); i++) {
                actions.add(new Action(room.gates.get(i),room.keys.get(i),new Room(room.rooms.get(i))));
            }

        }

        /**
         *
         * @return list of actions available for room.
         */
        public ArrayList<Action> getActions() {
            return actions;
        }

        @Override
        public boolean hasNext() {
            return  (actions.iterator().hasNext());
        }

        @Override
        public Iterator<Action> next() {
            return actions.iterator();
        }
    }
}
