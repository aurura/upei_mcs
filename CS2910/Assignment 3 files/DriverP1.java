/**
 * This will test problem 1 of Assignment 3.
 *
 * @author Gideon Landry
 * @since November 21, 2021
 */
public class DriverP1 {
    public static void main(String[] args) {

        // create a room named A and set all of its gates.
        Room a = new Room("A");
        a.setGate("west","","W");
        a.setGate("north","","N");
        a.setGate("south","","S");
        a.setGate("east","RED","E");

        // get iterator for room A.
        //Room.MazeRoomIterator actions = a.iterator();

        System.out.println("The following movements can be performed from room " + a.name());

        // list all actions for room A.
        for (Action action : a) {
            System.out.print("Using the " + action.gate + " gate we can access room " + action.room.name());
            if(action.key.equals(""))
                System.out.print(" without a key.\r\n");
            else
                System.out.print(" with a " + action.key + " key.\r\n");
        }

    }
}
