import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This will hold a linked structure that represents the maze map.
 *
 * @author Gideon Landry
 * @since November 21, 2021
 */
public class BidirectionalMap extends MazeMap {

    protected Room entrance = new Room("entrance");

    private ArrayList<Room> rooms = new ArrayList<>();
    public BidirectionalMap(String fileName) {

        try {

            Scanner input = new Scanner(new File(fileName));
            ArrayList<String> names = new ArrayList<>();
            ArrayList<String> gates = new ArrayList<>();
            ArrayList<String> keys = new ArrayList<>();

            // first loop through text file.
            while(input.hasNext()) {
                String[] line = new String[5];
                line[0] = input.next();
                line[1] = input.next();
                line[2] = input.next();
                if(line[2].charAt(0) == '<') {
                    line[3] = input.next();

                    // collect all room names.
                    if(!line[3].equals("Exit") && !line[3].equals("Entrance")) {
                        line[4] = input.next();
                        if(!gates.contains(line[4]))
                            gates.add(line[4]);
                    } else
                        names.add(line[3]);
                    // add gate to list
                    if(!gates.contains(line[1]))
                        gates.add(line[1]);

                } else
                    keys.add(line[1]); // add key from file

                // add name if not already in list.
                if(!names.contains(line[0]))
                    names.add(line[0]);

            }

            // for every room name create a room.
            for(String name : names)
                rooms.add(new Room(name));

            input = new Scanner(new File(fileName));

            // second loop through text file.
            while(input.hasNext()) {
                String[] line = new String[5];
                line[0] = input.next();

                // set current room to set gates for.
                Room current = new Room("");
                for(Room room : rooms)
                    if(room.name().equals(line[0]))
                        current = room;

                line[1] = input.next();
                line[2] = input.next();

                if (line[2].charAt(0) == '<') {
                    line[3] = input.next();

                    // this will hold key required for gate if needed.
                    String key = line[2].substring(1, line[2].length() - 1);;

                    if (!line[3].equals("Exit") && !line[3].equals("Entrance")) {
                        line[4] = input.next();

                        if (keys.contains(key))
                            current.setGate(line[1],key,line[3]);
                        else
                            current.setGate(line[1],"",line[3]);
                    } else
                        current.setGate(line[1],"",line[3]);
                }
            }

        } catch (FileNotFoundException e) {
            System.out.println("File error");
        }
    }
}
