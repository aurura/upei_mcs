/**
 * this will test problem 2
 *
 * @author Gideon Landry
 * @since November 21, 2021
 */
public class DriverP2 {
   public static void main(String[] args) {
        BidirectionalMap map = new BidirectionalMap("maze.txt");
        map.mazeReport();
   }
}
