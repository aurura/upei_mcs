package com.upei;

// Import the ArrayList
import java.util.ArrayList;
// We need this to sort!
import java.util.Collections;

public class ShelfList {

    //Exercise 4
    // need a couple of arbitrary variables

    // Exercise 4: Constructor for default 10 books
    /**
     * Create a new ShelfList object with 10 books.
     * Precondition: Object does not exist
     * Postcondition: Object is created with 10 books starting at the value in the first parameter.
     * @param startBook The id of the first book
     */
    public ShelfList(int startBook)
    {
        ShelfList[] shelfLists = {new Book(startBook)};
    }

    // Exercise 4: Constructor for arbitrary case or preset array case
    /**
     * Create a new ShelfList object with a variable number of books.
     * Precondition: Object does not exist
     * Postcondition: Object is created with the number of books passed in through the parameter.
     * @param bookID an array of values to create books from.
     */

    public ShelfList(int ... bookID)
    {
    }

    // Exercise 4: toString actually doesn't change!
    /**
     * Create a string containing the list of books belonging to the shelf.
     * Precondition: Object contains a number of books.
     * Postcondition: Object contains a number of books.
     * @return a string containing the IDs of all books belonging to the shelf.
     */

    public String toString()
    {
    }

    // Exercise 4: Updated removing a lost book - so much easier!
    /**
     * Remove the book found in the first parameter from the list.
     * Precondition: Object contains n books.
     * Postcondition: Object contains n books if the lostID is not on the shelf or n-1 books if it was found on the shelf.
     * @param lostID The ID of the book to remove
     * @return 0 if the book was removed correctly, -1 if the book was not found.
     */

    public int lostBook(int lostID)
    {
    }

    public static void main(String args[])
    {
        // Exercise 1: Input types based on scenarios
        ShelfList shelf1 = new ShelfList(3, 5, 7, 9, 12);
        ShelfList shelf2 = new ShelfList(33, 57, 14, 99, 22);
        ShelfList shelf3 = new ShelfList(100);

        // Exercise 1: Test cases
        String shelf1Out = shelf1.toString();
        if(shelf1Out.equals("3,5,7,9,12"))
            System.out.println("SUCCESS: Shelf 1 Books Correct");
        else
            System.out.println("ERROR: Shelf 2 Expected: 3,5,7,8,12; Received: "+shelf1Out);

        String shelf2Out = shelf2.toString();
        if(shelf2Out.equals("14,22,33,57,99"))
            System.out.println("SUCCESS: Shelf 2 Books Correct");
        else
            System.out.println("ERROR: Shelf 2 Expected: 14,22,33,57,99; Received: "+shelf2Out);

        String shelf3Out = shelf3.toString();
        if(shelf3Out.equals("100,101,102,103,104,105,106,107,108,109"))
            System.out.println("SUCCESS: Shelf 3 Books Correct");
        else
            System.out.println("ERROR: Shelf 3 Expected: 100,101,102,103,104,105,106,107,108,109; Received: "+shelf3Out);

        shelf3.lostBook(103);
        shelf3Out = shelf3.toString();
        if(shelf3Out.equals("100,101,102,104,105,106,107,108,109"))
            System.out.println("SUCCESS: Shelf 3 Books Correct After Lost Book");
        else
            System.out.println("ERROR: Shelf 3 Lost Book Expected: 100,101,102,104,105,106,107,108,109; Received: "+shelf3Out);

    }
}
