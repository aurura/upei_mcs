package com.upei;

public class Book {
    String title;
    String author;
    int id;

    // This is an interesting design decision to discuss, how to keep track of who has borrowed a book?
    private Boolean borrowed = false;
    private Client borrowedTo;

    public Book(String title, String author, int id){
        this.title = title;
        this.author = author;
        this.id = id;
    }

    public void borrowed(Client client){
        this.borrowed = true;
        this.borrowedTo = client;
    }

    public void returned(){
        this.borrowed = false;
        this.borrowedTo = null;
    }

    public Boolean isBorrowed() {
        return borrowed;
    }

    public int getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }
}
