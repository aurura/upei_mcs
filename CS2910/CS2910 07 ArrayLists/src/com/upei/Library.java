package com.upei;
public class Library {

    //Books collection (not static so it will be unique to each library instance)

    int capacity = 100;
    int numBooks = 0;
    Book[] books = new Book[capacity];

    // The Library name and website
    String name;

    public Library(String name){
        this.name = name;
    }

    /**
     * Adds a book to the Library's collection. The method adds the book if there is capacity and returns true
     * otherwise returns false
     * @param newBook the book
     * @return whether the book was added or not
     */
    public Boolean addBook(Book newBook){
        if (numBooks <= capacity){
            books[numBooks] = newBook;
            numBooks++;
            return true;
        }
        return  false;
    }


    /**
     * Borrows a book if the client hasn't reached the borrowing limit and if the book is not currently borrowed
     * @param book the book
     * @param client the client
     * @return true if the book was borrowed, false otherwise
     */
    public Boolean borrowRequest(Book book, Client client){
        if (client.getCurrentlyBorrowed() < client.getMaxBorrow() && !book.isBorrowed()){
            book.borrowed(client);
            client.borrow(book);
            return true;
        }
        return false;
    }

    /**
     * Borrows a book given a title
     * @param bookTitle the book title
     * @param client the client
     * @return true if the book was borrowed, false otherwise
     */
    public Boolean borrowRequest(String bookTitle, Client client){
        for (int i=0; i<this.numBooks; i++){
            if (this.books[i].title.equals(bookTitle) && !this.books[i].isBorrowed()) {
                return borrowRequest(this.books[i], client);
            }
        }
        return false;
    }

    /**
     * Returns a book to the library
     * @param book
     * @param client
     */
    public void returnBook(Book book, Client client){
        book.returned();
        client.giveBack(book);
    }

}
