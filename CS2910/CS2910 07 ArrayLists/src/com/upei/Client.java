package com.upei;

public class Client {
    private String name;
    private String address;

    protected int maxBorrow = 3;
    private int currentlyBorrowed = 0;
    private Book[] borrowed = new Book[10];

    public Client(String name, String address){
        this.address = address;
        this.name = name;
    }

    public void borrow(Book book){
        borrowed[currentlyBorrowed] = book;
        currentlyBorrowed++;
    }

    public void giveBack(Book book){
        // this would be much easier using a list
        for (int i=0; i<maxBorrow; i++){
            if (borrowed[i] == book){
                // if the book is found but there is more than one book borrowed we copy the last borrowed book
                // into this book's position in the array and make the last position null
                borrowed[i] = borrowed[currentlyBorrowed - 1];
                borrowed[currentlyBorrowed - 1] = null;
                currentlyBorrowed--;
            }
        }
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public int getCurrentlyBorrowed() {
        return currentlyBorrowed;
    }

    public int getMaxBorrow() {
        return maxBorrow;
    }

    /**
     * Returns a string with the titles of the books currently borrowed by the client
     * @return
     */
    public String borrowedTitles(){
        String booksTitles = "";
        for(int i=0; i<getCurrentlyBorrowed(); i++) {
            booksTitles += "\n"+borrowed[i].getTitle();
        }
        return booksTitles;
    }
}
