package com.upei;

public class ManagementSystem {
    public static void main(String[] args) {
        Book book1 = new Book("The Star Side of Bird Hill", "Naomi Jackson", 1);
        Book book2 = new Book("The House of Purple Cedar", "Tim Tingle", 2);
        Book book3 = new Book("The Ballad of Black Tom", "Victor LaValle", 3);
        Book book4 = new Book("Murder on the Red River", "Marcie R. Rendo", 4);

        Client client = new Client("Antonio", "Cass Science Building");

        Library upeiLibrary = new Library("Robertson Library");

        upeiLibrary.addBook(book1);
        upeiLibrary.addBook(book2);
        upeiLibrary.addBook(book3);
        upeiLibrary.addBook(book4);

        // attempting to borrow 4 books, only 3 should be borrowed
        System.out.println(upeiLibrary.borrowRequest("The Star Side of Bird Hill", client));
        System.out.println(upeiLibrary.borrowRequest("The House of Purple Cedar", client));
        System.out.println(upeiLibrary.borrowRequest("The Ballad of Black Tom", client));
        System.out.println(upeiLibrary.borrowRequest("Murder on the Red River", client));

        // We return the first book that was borrowed and then print the list of books
        // that the client still has
        upeiLibrary.returnBook(book1, client);

        System.out.println(client.borrowedTitles());
        // returning books
        upeiLibrary.returnBook(book2, client);
        upeiLibrary.returnBook(book3, client);


        //Exercise: VIP Client
        System.out.println("\nNow with a VIP client:");
        VIPClient vipClient = new VIPClient("Chris", "Cass Science Building");
        // attempting to borrow 4 books, all 4 should succeed
        System.out.println(upeiLibrary.borrowRequest("The Star Side of Bird Hill", vipClient));
        System.out.println(upeiLibrary.borrowRequest("The House of Purple Cedar", vipClient));
        System.out.println(upeiLibrary.borrowRequest("The Ballad of Black Tom", vipClient));
        System.out.println(upeiLibrary.borrowRequest("Murder on the Red River", vipClient));





    }
}
