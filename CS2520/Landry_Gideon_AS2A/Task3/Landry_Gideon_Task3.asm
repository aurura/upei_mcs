# AS2A Task 3
# Calculate sum 
# 
# Author: Gideon Landry
# Date: 24-MAR-2021 

.data
	newline:  .asciiz "\n"	# newline 

.text 
main:	
	li $t0, 0 # sum
	li $t1, 1 # X
	li $t2, 0 # temp

	loop_x:
		li $t3, 1 				# Y 
	loop_y:
		add $t0, $t0, $t1		# add X to sum
		add $t0, $t0, $t3		# add Y to sum
		add $t2, $t1, $t3		# add X and Y to temp
		move $a0, $t2			# prepare sum of X and Y for printing
		li $v0, 1				# set $v0 syscall to print int
		syscall					# print
		la $a0, newline			# prepare newline character for printing 
		li $v0, 4				# set $v0 syscall to print string
		syscall					# print
		add $t3, $t3, 1			# increment Y
		ble $t3, 4, loop_y		# if Y is 4 or less jump to start of loop_y
		move $a0 $t0			
		li $v0, 1
		syscall
		la $a0, newline			
		li $v0, 4				
		syscall					
		add $t1, $t1, 1			# increment X
		ble $t1, 25, loop_x		# if X is 25 or less jump to start of loop_x
		li $v0, 10				# set $v0 syscall to exit
		syscall					# exit

	