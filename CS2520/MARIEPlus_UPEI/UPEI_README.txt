Dear UPEI Student,

This is a modification of the standard MARIE distribution v3 by Null and Lobur.

It contains an extra opcode that is NOT covered in your textbook:

LoadA <operand>

where <operand> is one of:

a label (which will be taken from the symbol table)

or

a direct hexadecimal value (and only hexadecimal - you cannot use decimal directly)

This enables you to declare a chunk of memory as an array, and use LoadA to load its starting address.

This is a necessary prelude to being able to use the SPIM assembler (MIPS emulator) later in the course.

The MarieSim program (MARIE virtual machine and editor/assembler) has been updated to process this opcode (which is opcode 15/0xF)

The MARIE Datapath (MarieDPath) has been similarly updated to show the processing of this opcode

13-JAN-2021 CRV

