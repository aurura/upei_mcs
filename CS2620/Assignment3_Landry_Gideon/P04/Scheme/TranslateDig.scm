(define num 2)

(define findword 
    (lambda (num numlist wordlist) 
        (if (= (car numlist) num)
            (car wordlist)
            (findword num (cdr numlist) (cdr wordlist))
        )
    )
)

(define translatedig
    (lambda (num)
        (format #t "~d~%" (findword num '(0 1 2 3 4 5 6 7 8 9) '(zero one two three four five six seven eight nine)))
    )
)


(translatedig 6)
(translatedig 8)
