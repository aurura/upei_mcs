(defun findword (num numlist wordlist) 
    (if (= (car numlist) num)
        (car wordlist)
        (findword num (cdr numlist) (cdr wordlist))
    )
)

(defun translatedig (num)
    (format t "~d~%" (findword num '(0 1 2 3 4 5 6 7 8 9) '(zero one two three four five six seven eight nine)))
)

(translatedig 9)
(translatedig 4)