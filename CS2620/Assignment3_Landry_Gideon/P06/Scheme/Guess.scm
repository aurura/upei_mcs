(define key '(13 39 40))

(define guess 
    (lambda (n k)
        (if (> k 0)
            (if (member n key)
                (format #t "CORRECT!!!!")
                (format #t "Try Again.")
            )
            (format #t "Reached Maximum Number of Attempts!")
        )
    )
    (if (> k 0) (guess (+ n 1) (- k 1)))
)

(guess 13 1)
