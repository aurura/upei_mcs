(defvar *key* '(13 39 40))

(defun guess (n k)
    (if (> k 0)
        (if (member n *key*)
            (print "CORRECT!!!!")
            (print "Try Again.")
        )
        (print "Reached Maximum Number of Attempts!")
    )
    (if (> k 0) (guess (+ n 1) (- k 1)))
)

(guess 13 1)