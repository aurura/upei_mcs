(defun checkprime (prime divi)
    (if (> divi 1)
        (if (= 0 (mod prime divi))
            nil   ;return "false"
            (checkprime prime (- divi 1))
        )     
        1   ; return "true"
    )
)

(defun findsum (value sum)
    (if (> value 2) 
        (progn 
            (if (checkprime value (floor (/ value 2)))
                (setf sum (+ sum value))
            ) 
            (findsum (- value 1) sum)
        )   
        (+ sum 2)
    )
)

(defun primesum (value)
    (format t "The sum of all prime numbers less than ~d~% is ~d~%" value (findsum value 0))
)  

(primesum 20)
(primesum 35)



