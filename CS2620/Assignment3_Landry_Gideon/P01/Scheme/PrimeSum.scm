(define checkprime
    (lambda  (prime divi)
        (if (> divi 1)
            (if (= 0 (mod prime divi))
                #f   ;return "false"
                (checkprime prime (- divi 1))
            )     
            1   ; return "true"
        )
    )
)

(define findsum 
    (lambda (value sum)
        (if (> value 2) 
            (begin 
                (if (checkprime value (floor (/ value 2)))
                    (set! sum (+ sum value))
                ) 
                (findsum (- value 1) sum)
            )   
            (+ sum 2)
        )
    )
)

(define primesum 
    (lambda (value)
        (format #t "The sum of all prime numbers less than ~d~% is ~d~%" value (findsum value 0))
    )
)  

(primesum 20)
(primesum 35)
