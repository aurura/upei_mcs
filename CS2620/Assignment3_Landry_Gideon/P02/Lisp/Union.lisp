(defun combine (list1 list2)
    (format t "~S~%" (append list1 list2))
)

(combine '(1 2 3 4 5) '(a b c d e))

(combine '(a b c d e f) '(s d g e w w))

