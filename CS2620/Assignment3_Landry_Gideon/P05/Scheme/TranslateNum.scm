(define numbers '(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 30 40 50 60 70 80 90))
(define words '(one two three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty thirty forty fifty sixty seventy eighty ninety))
(define totalphrase '())

(define findphrase 
    (lambda (num numlist wordlist)
        (if (> (length numlist) 0)
            (if (= num (car numlist))
                (set! totalphrase (append totalphrase (cons (car wordlist) '())))  
                (begin
                    (if (> num 20)
                        (if (> (mod num 10) 0)
                            (begin
                                (findphrase (* (floor (/ num 10)) 10) numbers words)      
                                (findphrase (mod num 10) numbers words)
                                (set! numlist '())
                            )
                        )    
                    )    
                    (findphrase num (cdr numlist) (cdr wordlist))
                )    
            )
        )
    )
)

(define translatenum 
    (lambda (num)
        (findphrase num numbers words)
        ;(findtranslation num  '(1000000000 1000000 1000 100) '(billion million thousand hundred))
        (format #t "~S~%" totalphrase)
        (set! totalphrase '())
    )
)

(translatenum 5)

