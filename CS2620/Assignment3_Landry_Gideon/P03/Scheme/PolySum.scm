(define *sum* 0)

(define poly 
    (lambda (value)
        (set! *sum* (+ *sum* (expt (* value 3) 2)))
    )
)

(define findsum 
    (lambda (num) 
        (if (>= num 0)     
            (poly num)
        )
        (if (>= num 0) 
            (findsum (- num 1))
        )
    )   
)

(define polysum 
    (lambda (num)
        (findsum num)
	(format #t "~d~%" *sum*)
    )
)

(polysum 4)
(polysum 8)
