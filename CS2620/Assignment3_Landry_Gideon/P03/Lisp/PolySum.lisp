(defvar *sum* 0)

(defun poly (value)
    (setf  *sum* (+ *sum* (expt (* value 3) 2)))
)

(defun findsum (num) 
    (if (>= num 0)     
        (poly num)
    )
    (if (>= num 0) 
        (findsum (- num 1))
    )
)

(defun polysum (num)
    (findsum num)
    (format t "~d~%" *sum*)
)

(polysum 4)
(polysum 8)
