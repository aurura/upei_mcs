%is_integer(X):- nonvar(X), X>=1,Y is X-1, is_integer(Y).
%is_integer(X):- nonvar(X), X<0,Y is -X, is_integer(Y).

%is_integer(0).
%is_integer(1).

is_integer(X):- length(_, X).

divide(N1, N2, Result) :-
is_integer(Result),
P1 is Result * N2,
P2 is (Result + 1) * N2,
P1 =< N1, P2 > N1,
! .

