ancestor(mary, shelley) :- mother(mary, shelley).
grandfather(X,Z) :- father(X,Y),parent(Y,Z).
parent(X,Y) :- father(X,Y).
parent(X,Y) :- mother(X,Y).
father(X,mary).
mother(sue, mary).

grandfather(X,Z), father(Z,mary).
