p(a).
p(b).
q(c).
      
?-not p(X), q(X).   % fails
?-q(X), not p(X).   % succeeds with X=c
