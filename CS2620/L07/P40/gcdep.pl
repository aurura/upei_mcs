gcd(X,X,X).
gcd(X,Y,D):- X<Y, Y1 is Y - X, gcd(X,Y1,D).
gcd(X,Y,D):- X>Y, gcd(Y,X,D).



main :- gcd(30,40,X),format('GCD(30,40)='),write(X),nl,
        gcd(20,54,Y),format('GCD(27,3078)='),write(Y),nl,
		format('Enter a number a='),read(N1),
		format('Enter the second number b='),read(N2),
		gcd(N1,N2,N3),
                format('GCD('),write(N1),format(','),write(N2),format(')='),
		write(N3),format('~n').
