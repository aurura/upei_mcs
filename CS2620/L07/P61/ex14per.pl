
del(X,[X|T],T).
del(X,[Y|T],[Y|T1]):- del(X,T,T1).

insert(X,Y,Z):-del(X,Z,Y).


permute1([],[]).
permute1([H|T],P) :- permute1(T,P1), insert(H,P1,P).
