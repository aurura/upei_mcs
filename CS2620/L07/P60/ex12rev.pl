reverse([],[]).
reverse([H|Tail],R) :- reverse(Tail,R1), append(R1,[H],R).
