subset([],S).
subset([H,Tail],S) :- member (H,S), subset(Tail,S).
