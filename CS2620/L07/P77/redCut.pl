
lookup(K,V,[(K,W)|_]) :- !, V=W.
lookup(K,V,[_|Z]) :- lookup(K,V,Z).

install(K,V,[(K,W)|_]) :- V=W.
install(K,V,[_|Z]) :- install(K,V,Z).
