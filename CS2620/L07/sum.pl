odd1(N):- N >= 0, 1 is (N mod 2).
odd1(N):- N < 0, N1 is -N, odd(N1).



test(N):-odd1(N).

sum(0,0).
sum(1,1).
sum(N,S):-N1 is N - 1, test(N), sum(N1,S1), S is S1+N.
sum(N,S):-N1 is N - 1,  sum(N1,S1), S is S1.

sumlt(N,S):-N1 is N - 1, sum(N1,S).
