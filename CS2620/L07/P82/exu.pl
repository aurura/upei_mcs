not(X):- X, !, fail.
not(_).




mother(mary, tyler).
father(scott, tyler).
blue_eyed(tyler).
non_parent(X, Y) :- not(father(X,Y)), not(mother(X,Y)).
