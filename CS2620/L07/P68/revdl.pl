rev_dl([],L-L). 
rev_dl([X],[X|L]-L).
rev_dl([H|T],L1-L3):- rev_dl(T,L1-L2).
