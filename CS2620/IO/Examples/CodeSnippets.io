// capital letter object
// lowercase lower case is an instance

// create a new object named MyObject
MyObject := Object clone

// add a slot that prints a description
MyObject description := "this is my object" println

// add a method slot
MyObject methodslot := method(writeln("This is a method"))

MyObject slotNames

// ==> list("description", "methodslot")

MyObject protos 

// ==> list("Object")

// create an instance of MyObject
newObject := MyObject clone

Dog := Object clone

Dog bark := "Bark" println // give object slot

Dug := Dog clone

Dug bark // prints Bark

Dug protos // display object elements

BDog := Dog clone

Dog Bdog = method(writeln("Bark"); resend) // print bark 2 times // methods are lists?

Object clone := "No more cloning" // may crash. override

// Object  //slots = predefined objects/methods

dog2 barking := method(writeln("WOOF!"); super(bark))

// lists all operators
OperatorTable

// adds ! to operator table  
OperatorTable addOperator("!", 3) 

// make the 
! := method(boolean, if(boolean, false, true)) 

loop("hello" println) // loop forever

// prints hello 5 times
5 repeat("hello" print) 

// for loop
for(i, 1, 10, i print) 

//if // lazy evaluation?

if(num % 2 == 0, "number is even" print, "number is odd" print)

// can't clone bool true

// declare list
MyList := list("a", "b","list") 

// add 5 to end of list
MyList append(5)

// add 1 to front of list
MyList prepend(1) 

// return last element in list and remove it from list
MyList pop 

Dave := List clone // declares empty list

DaveMap := Map clone // declare new map

DaveMap atput("a",1) // add key(a) with value 1

DaveMap atput("Map",2) // add key(Map) with value 2

DaveMap asList // returns Map as a list of lists

DaveMap at("a") // returns value with key a in map

// other list methods exist

First := Object clone

Second := Object clone

wait(1) // wait 1 second

First go := method(wait(1);"Im First" println)

Second go := method(wait(2); "Im Second" println)

Second go; First go // prints second go first then first

Second @go; First @go; wait(3) // @ allows subroutine to run the wait(3) allows time for the subroutine to finish

// polymorphic

// numeric value
numVal := 1

// string value
strVal := "hello world"

// basic method that prints Hello world and returns true.
myMethod1 := method(writeln("Hello world"); true)

// this method takes x and y as paramaters and returns their product.
myMethod2 := method(x, y, x * b)