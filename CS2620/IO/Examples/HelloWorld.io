Barrier := Object clone
Barrier clientNum := 0
Barrier clientList := List clone
Barrier clientArrive := method(clientName,
                            clientList append(clientName);
                            "#{clientName} has arrived" interpolate println
                             clientNum = clientNum - 1
                            if(clientNum == 0, done) // call done method when no more clients
                            )
Barrier done := method(clientList foreach(v, Client leaving(v)))

Client := Object clone
Client cNum ::= 0 // new slote in Client (creates a setter)
Client ready := method(num,
                    wait(1)
                    Wild
                    Barrier clientArrive("Client #{cNum}" interpolate)
                    )
Client Wild := method(if(cNum % 2 == 0, wait(2)))
Client leaving := method(clientName, "Client #{cName} is leaving" interpolate println)

sNums := File standardInput readLine
nums := sNums asNumber
Barrier clientNum = nums

for(i, 1, nums,
    iClient := Client clone setCNum(i) // create a new instance of Client setting cNum
    iClient @ready(i)
    "Started a client" println
)
Coroutine currentCoroutine pause
// deadlock protection ends program