item(0, fries, 8, 5).
item(1, hamburgers, 10, 5).
item(2, salads, 6, 5).
item(3, hotdogs, 5, 5).
item(4, chickenburgers, 11, 5).

menu(X) :- calculate(Y, 0), X is Y.

price(X) :- write('Bill: $'),write(X).

calculate([],5) :-!.
calculate(Y, C) :- C2 is C + 1,
                calculate(Y2, C2),
                item(C, I, P, T),
                write('Number of '),
                write(I),
                write('? '),
                read(Q),
                Q > T, X is P * T; 
                X is P * Q,
                Y is X + Y2.
        
