secret(X,[X|_]) :- write('Element is in list!'), !.
secret(X,[_|T]) :- secret(X,T).

guess(_,0) :- write('You Lose.').
guess(X,K) :- secret(X,[d,g,b,e]); (read(X2), Y is K - 1, guess(X2,Y)).