word(1000000000,[billion]).
word(1000000,[million]).
word(1000,[thousand]).
word(100,[hundred]).
word(90,[ninety]).
word(80,[eighty]).
word(70,[seventy]).
word(60,[sixty]).
word(50,[fifty]).
word(40,[forty]).
word(30,[thirty]).
word(20,[twenty]).
word(19,[nineteen]).
word(18,[eighteen]).
word(17,[seventeen]).
word(16,[sixteen]).
word(15,[fifteen]).
word(14,[fourteen]).
word(13,[thirteen]).
word(12,[twelve]).
word(11,[eleven]).
word(10,[ten]).
word(9,[nine]).
word(8,[eight]).
word(7,[seven]).
word(6,[six]).
word(5,[five]).
word(4,[four]).
word(3,[three]).
word(2,[two]).
word(1,[one]).

generate(0, [], _) :- !.
generate(N, P2, N2) :- (N >= 100), word(X,Y), (T is ( N // X )), T >= 1, 
                    generate(T, W2, _), append(W2, Y, P1), 
                    N2 is N - (T * X), 
                    generate(N2, W3, _), append(P1, W3, P2),!.
generate(N, W, _) :- word(N, W); N2 is (N // 10)*10, N3 is N - N2, word(N2, W2),word(N3, W3),append(W2, W3, W).

number(0, [zero]) :- !.
number(N, P) :- generate(N, P, _).