numword(0,zero).
numword(1,one).
numword(2,two).
numword(3,three).
numword(4,four).
numword(5,five).
numword(6,six).
numword(7,seven).
numword(8,eight).
numword(9,nine).

translate([],[]).
translate([NumHead|NumTail],[WordHead|WordTail]) :-
numword(NumHead,WordHead),
translate(NumTail,WordTail).


