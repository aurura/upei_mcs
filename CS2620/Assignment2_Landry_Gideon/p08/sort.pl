sortnums(L1,L2,LM) :- append(L1,L2,L3),reverse(L3,LM).

reverse([ ], [ ]).
reverse([H|Tail], R) :- reverse(Tail, R1), append(R1,[H],R).