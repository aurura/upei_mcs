secret(744) :- write('744 is correct').

guess(X) :- write('Guess a number between 100 and 1000: '),
            read(X),
            guess(X, 4).

guess(_,0) :- write('You Lose.'), !.
guess(X,K) :- secret(X); write('Try again: '), read(X2), Y is K - 1, guess(X2,Y).


