primesum(2,2) :- !.
primesum(X,Y) :- X2 is X - 1, primesum(X2, Y2), D is X // 2, checkprime(X, D, Y3), Y is Y2 + Y3.

checkprime(X,1,X) :- !.
checkprime(X,D,Z) :- X mod D =:= 0, Z is 0; D2 is D - 1, checkprime(X, D2, Z).

