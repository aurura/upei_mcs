#!/bin/bash
# Author Gideon Landry
# Date April 12, 2021

echo "Calculate the summation of a function."

echo "Compiling..."

cc SumPolyFunc.c

echo "Running"

./a.out

echo "cleaning"

rm a.out

