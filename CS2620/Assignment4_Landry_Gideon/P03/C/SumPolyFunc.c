/**
 * This program will calculate the sum from 0 to n of an polynomial function.
 * 
 * Author: Gideon Landry
 * Date: April 6, 2021
 */
#include <stdio.h>

int func(int x);
int pow(int x, int y);
int sigma(int (*f)(int),int n);

int main(void)
{
    int n;
    printf("f(x) = 3x^4 + 11x^3 + 2x^2 + 4x \r\n");
    printf("Enter length of sequence(n): ");
    scanf("%d", &n);
    printf("Summation equals: %d \r\n",sigma(func,n));
}

// calculate x to the power of y.
int pow(int x, int y)
{
    int prod = 1;
    for(int i = 0; i < y; i++)
    {
	prod = prod * x;
    }
    return prod;
}

// f(x) = 3x^4 + 11x^3 + 2x^2 + 4x
int func(int x)
{
    int result = (3 * pow(x, 4.0) + (11 * pow(x, 3.0)) + (2 * pow(x, 2.0)) + (4 * x));
    return result;
}

// calculate summationg of input function f(x) + f(x + 1).
int sigma(int (*f)(int),int n)
{
    int sum = 0;
    for(int i = 0; i <= n; i++)
    {
        sum = sum + ((*f)(i) + (*f)(i + 1));
    }
    return sum;
}
