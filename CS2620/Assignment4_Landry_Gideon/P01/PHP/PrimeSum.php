<?php
    /*
        Calculate the sum of all prime numbers below a given number.

        Author: Gideon Landry
        Date: April 5, 2021
    */

    $sum = 0;

    // check if number is prime
    function checkPrime($prime, $divi)
    {  
        $isPrime = true;
        for($i = 2; $i <= $divi; $i++)
        {
            if(($prime % $i) == 0)
            {
                $isPrime = false;
                $i = $divi + 1;
            }
        }
        return $isPrime;
    }

    function findSum($value)
    {
        global $sum;
        if($value > 2)
        {
            if(checkPrime($value, (int)floor($value / 2)))
            {
                $sum = $sum + $value;
            }
            findSum(($value - 1), $sum);
        }
        else
        {
            $sum = $sum + 2;
        }
    }

    $inVal = (int)readline('Enter number: ');
    findSum($inVal - 1);
    echo "Sum of prime numbers below ".$inVal." is ".$sum."\r\n";
 ?>
