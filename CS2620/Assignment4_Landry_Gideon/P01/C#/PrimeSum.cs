/**
    Calculate the sum of all prime numbers below a given number.

    Author: Gideon Landry
    Date: April 5, 2021
 */
using System;
using System.IO;

class PrimeSum
{

    public static int sum = 0;

    static void Main(string[] args)
    {
        int prime;

        Console.WriteLine("Enter Number: ");
        prime = Convert.ToInt32(Console.ReadLine());
        findSum((prime - 1));
        Console.WriteLine("The sum of all prime numbers below " + prime + " is " + sum);

    }
        
    // check if number is prime
    public static bool checkPrime(int prime, int divi)
    {
        bool isPrime = true;
        for(int i = 2; i <= divi; i++)
        {
            if((prime % i) == 0)
            {
                isPrime = false;
                i = divi + 1;
            }
        }
        return isPrime;
    }

    // calculate sum of prime numbers 
    public static void findSum(int value)
    {
        if(value > 2)
        {
            if(checkPrime(value, Convert.ToInt32(Math.Truncate(Convert.ToDecimal(value / 2)))))
            {
                sum = sum + value;
            }
            findSum((value - 1));
        }
        else
        {
            sum = sum + 2;
        }
        return;
    }
}