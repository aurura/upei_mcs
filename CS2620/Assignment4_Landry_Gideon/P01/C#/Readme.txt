#!/bin/bash

#creating an executable from a cs file
# Author Gideon Landry
# Date April 12, 2021

echo "Calculate sum of primes."

mcs PrimeSum.cs

./PrimeSum.exe
rm PrimeSum.exe

