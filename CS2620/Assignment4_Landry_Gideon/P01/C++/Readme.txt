#!/bin/bash
#creating an executable from a c program with c++
# Author Gideon Landry
# Date April 12, 2021
echo "Calculate sum of primes."

c++ PrimeSum.c++

./a.out
rm a.out