/**
 * This program will calculate the sum of all prime numbers less then a given number.
 * 
 * Author: Gideon Landry
 * Date: April 5, 2021
 */
#include <iostream>
#include <cmath>
using namespace std;

class Prime
{

    public:
        int prime;
        int sum = 0;
        

        Prime(int inprime)
        {
            prime = inprime;
            findSum((prime - 1));
            printf("The sum of all prime numbers below %d is %d", prime, sum);
        }

        // check if number is prime
        int checkPrime(int prime, int divi)
        {
            bool isPrime = true;
            for(int i = 2; i <= divi; i++)
            {
                if((prime % i) == 0)
                {
                    isPrime = false;
                    i = divi + 1;
                }
            }
            return isPrime;
        }

        // calculate sum of prime numbers 
        void findSum(int value)
        {
            if(value > 2)
            {
                if(checkPrime(value, floor(value / 2)))
                {
                    sum = sum + value;
                }
                findSum((value - 1));
            }
            else
            {
                sum = sum + 2;         
            }
        }

};

int main()
{
    
    cout << "Enter Number: ";
    int prime;
    cin >> prime;

    Prime primesum(prime);

}



