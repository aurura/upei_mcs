(*
    Calculate the sum of all prime numbers below a given number.

    Author: Gideon Landry
    Date: April 6, 2021
*)

program primeSum(input, output);

var
    invalue, i, sum, divi: integer;
    isprime: boolean;

// calculate sum.
function findSum(value: integer): integer;
begin
    if(value > 2) then
        begin
	    divi := Trunc(value/2);

	    // check if value is prime.
	    isprime := true;
	    for i := 2 to divi do
	    begin
		if((value mod i) = 0) then
		    isprime := false;
	    end;
            if(isprime) then
                sum := sum + value;
            findSum(value - 1);
        end
    else
        sum := sum + 2;  
end;

begin
    writeLn('Enter number: ');
    read(input, invalue);
    findSum(invalue - 1);
    writeLn('The sum of prime numbers is ',sum);
end.
