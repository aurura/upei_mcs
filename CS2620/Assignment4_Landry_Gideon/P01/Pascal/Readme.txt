#!/bin/bash
#compiled pascal file using fpc.
# Author Gideon Landry
# Date April 12, 2021

echo "Calculate sum of primes."

fpc PrimeSum.pas

./PrimeSum

rm PrimeSum
rm PrimeSum.o
