! Calculate the sum of all prime numbers below a given number.
! Author: Gideon Landry
! Date: April 6, 2021

        program primesum
            integer :: t, i, invalue, tsum, divi
            logical :: isprime

	        tsum = 0;
            print*, "Enter Number: "
            read*, invalue
            do t = 1,(invalue - 2)
                isprime = .TRUE.
                divi = int(floor(real(invalue - t) / 2))
                do i = 2,divi
                    if(modulo((invalue - t),i) == 0) then
                        isprime = .FALSE.
                    end if
                end do
                if(isprime) then
                    tsum = tsum + (invalue - t)
                    
                end if
            end do
            print*, "sum of primes: ",tsum 
        end program primesum

