#!/bin/bash
# compile fortran file using f95
# Author Gideon Landry
# Date April 12, 2021

echo "Calculate sum of primes."

f95 PrimeSum.f -o program

./program
rm program