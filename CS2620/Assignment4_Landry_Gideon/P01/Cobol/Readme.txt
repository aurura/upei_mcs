#!/bin/bash

#creating an executable from a Cobol program with cobc.
# Author Gideon Landry
# Date April 12, 2021

echo "Calculate sum of primes."

# Compiling
cobc -free -x -o ps PrimeSum.cbl

# Executing
./ps

# removing the binary file
rm ps
