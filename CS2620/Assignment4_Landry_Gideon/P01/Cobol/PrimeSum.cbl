       IDENTIFICATION DIVISION. 
       PROGRAM-ID. PRIMESUM.
       DATA DIVISION.
           WORKING-STORAGE SECTION.
           01 PRIME PIC 9(7).
           01 DIVI PIC 9(7).
           01 VALU PIC 9(7).
           01 TSUM PIC 9(7).
           01 R PIC 9(7).
           01 T PIC 9(7).
       PROCEDURE DIVISION.
           DISPLAY 'ENTER A NUMBER: '.
           ACCEPT PRIME.
           MOVE PRIME TO VALU.
	       SUBTRACT 1 FROM VALU.
           PERFORM FINDSUM UNTIL VALU<3.
	       IF PRIME > 3
	         ADD 3 TO TSUM.
	       IF PRIME > 2
		     ADD 2 TO TSUM.
           DISPLAY 'The sum of prime numbers below ' PRIME ' is ' TSUM.
             STOP RUN.

           FINDSUM. DIVIDE VALU BY 2 GIVING DIVI.
             PERFORM CHECKPRIME UNTIL DIVI=1.
             SUBTRACT 1 FROM VALU.
           CHECKPRIME. DIVIDE VALU BY DIVI GIVING T REMAINDER R.
	         IF R = 0
		       SET DIVI TO 2.
             SUBTRACT 1 FROM DIVI.
             IF DIVI = 1 AND NOT R = 0
               ADD VALU TO TSUM.
