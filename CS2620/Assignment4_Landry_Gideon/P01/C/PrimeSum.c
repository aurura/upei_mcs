/**
 * This program will calculate the sum of all prime numbers less then a given number.
 * 
 * Author: Gideon Landry
 * Date: April 5, 2021
 */
#include <stdio.h>
#include <math.h>

// prototypes
int checkPrime(int prime, int divi);
int findSum(int value, int sum);

int main(void)
{

    int prime;
    printf("Enter Number: ");
    scanf("%d", &prime);
    int sum = findSum((prime - 1),0);
    printf("The sum of all prime numbers below %d is %d \r\n", prime, sum);

}

// check if number is prime
int checkPrime(int prime, int divi)
{
        int isPrime = 1;
        for(int i = 2; i <= divi; i++)
        {
            if((prime % i) == 0)
            {
                isPrime = 0;
                i = divi + 1;
            }
        }
    
    return isPrime;
}

// calculate sum of prime numbers
int findSum(int value, int sum)
{
    if(value > 2)
    {
        if(checkPrime(value, (int)floor(value / 2)))
        {
            sum = sum + value;
        }
        findSum((value - 1), sum);
    }
    else
    {
        sum = sum + 2;
        return sum;
    }
}

