#!/bin/bash
#creating an executable from a C# program with mcs
# Author Gideon Landry
# Date April 12, 2021

echo "Compiling..."

mcs Hangman.cs

echo "Running"

./Hangman.exe

echo "cleaning"

rm Hangman.exe