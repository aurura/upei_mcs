/**
    Calculate the sum of all prime numbers below a given number.

    Author: Gideon Landry
    Date: April 10, 2021
 */
using System;
using System.IO;

class hangman
{
    static void Main(string[] args)
    {
        const int NUMWRONG = 6; 

        int choice;
        int length = 0;
        int correct = 0;
        int incorrect = 0;
        bool found = false;
        char letter;

        // holds all hangman words.
        string[] guess = {"hello ", "world ", "star ", "function ", "grab ", "mars ", "delta ", "sigma ", "flow ", "lucky "};
        char[] word;
        char[] guesses;

        Console.WriteLine("Choose Word (1-10): ");
        choice = Convert.ToInt32(Console.ReadLine());

        word = guess[choice - 1].ToCharArray();

        // get length of the word choosen.
        foreach(char i in word)
        {
            if(!char.IsWhiteSpace(i))
            length++; 
        }

        guesses = new char[length];

        // initialize guesses with "*".
        for(int i = 0; i < length; i++)
        {
            guesses[i] = '*';
        }

        // begin hangman.
        while(incorrect < 6 && correct < length)
        {
            for(int i = 0; i < length; i++)
            {
                Console.Write(guesses[i]);
            }

            // get letter from user.
            Console.WriteLine("\r\nGuess letter.\r\n");
            letter = Console.ReadLine()[0];

            // check if letter is in word.
            for(int i = 0; i < length; i++)
            {
                found = false;
                if(word[i] == letter)
                {
                    guesses[i] = letter;
                    correct++;
                    found = true;
                }
            }

            // if letter was not found, increment incorrect. 
            if(!found)
            {
                incorrect++;
            }

        }

        if(correct >= length)
        {
            Console.WriteLine("You Won!");
            foreach(char i in guesses)
            {
                Console.Write(i);
            }
        }
        else
        {
            Console.WriteLine("You Lose!");
        }
    }
}
