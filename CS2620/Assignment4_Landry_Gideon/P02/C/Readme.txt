#!/bin/bash
#creating an executable from a c program with cc
# Author Gideon Landry
# Date April 12, 2021

echo "Compiling..."

cc Hangman.c

echo "Running"

./a.out

echo "cleaning"

rm a.out

