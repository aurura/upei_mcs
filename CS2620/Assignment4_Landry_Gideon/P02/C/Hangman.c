/**
 * hangman.
 * 
 * Author: Gideon Landry
 * Date: April 9, 2021
 */
#include<stdio.h>
#define MAX 8
#define NUMWRONG 6

// prototypes
int checkPrime(int prime, int divi);
int findSum(int calue, int sum);

int main(void)
{

    _Bool found = 0;
    int incorrect = 0;
    int correct = 0;
    int length  = 0;
    char guesses[MAX]; // this will hold guesses the user gives.

    // holds all hangman words.
    char guess[10][MAX] = {{'h','e','l','l','o'}, {'w','o','r','l','d'}, {'s','t','a','r'}, {'f','u','n','c','t','i','o','n'}, {'g','r','a','b'}, {'m','a','r','s'}, {'d','e','l','t','a'}, {'s','i','g','m','a'}, {'f','l','o','w'}, {'l','u','c','k','y'}};

    // get word choice from user.
    printf("Choose Word (1-10): ");

    // hold word number.
    int s;
    scanf("%d", &s);

    s--;

    // get length of the word choosen.
    for(int i = 0; i < MAX; i++)
    {
        if(guess[s][i])
        {
            length++;
        }
    }

    // initialize guesses with "*".
    for(int i = 0; i < length; i++)
    {
        guesses[i] = '*';
    }

    char letter;

    // begin hangman.
    while(incorrect < NUMWRONG && correct < length)
    {
        for(int i = 0; i < length; i++)
	    {
            printf("%c",guesses[i]);
	    }

        // get letter from user.
        printf("\r\nGuess letter: \r\n");
	    scanf(" %c", &letter);

        // check if letter is in word.
        for(int i = 0; i < length; i++)
        {
            found = 0;
            if(guess[s][i] == letter)
            {
                guesses[i] = letter;
                correct++;
                found = 1;
            }
        }

        // if letter was not found, increment incorrect.
        if(found == 0)
        {
            incorrect++;
        }
    }
}



