/**
 * This program will calculate the sum of all prime numbers less then a given number.
 * 
 * Author: Gideon Landry
 * Date: April 5, 2021
 */
#include <iostream>
using namespace std;


class Word
{
    public:
        int length;
        int strikes = 0;
        int correct = 0;
        char letter;
        char word[8];
        char guesses[8];

        Word(int length, char inWord[])
        {
            this->length = length;
            init(inWord);
            while(strikes < 6 && correct < length)
            {
                for(int i = 0; i < length; i++)
                {
                    cout << guesses[i];
                }

                 // get letter from user.
                cout << "\r\nGuess letter: \r\n";
                cin >> letter;

                // check if letter is in word.
                if(!findLetter(letter))
                {
                    strikes++;
                }
            }
        }

        // check if number is prime
        void init(char inWord[])
        {
            // initialize guesses with "*".
            for(int i = 0; i < length; i++)
            {
                word[i] = inWord[i];
                guesses[i] = '*';
            }
        }

        // check if letter is in word.
        bool findLetter(char letter)
        {
            bool found;
            for(int i = 0; i < length; i++)
            {
                found = false;
                if(word[i] == letter)
                {
                    guesses[i] = letter;
                    correct++;
                    found = true;
                }
            }
            return found;
        }

};

int main(void)
{

    int length = 0;
    int s; // hold word number.

    // holds all hangman words.
    char guess[10][8] = {{'h','e','l','l','o'}, {'w','o','r','l','d'}, {'s','t','a','r'}, {'f','u','n','c','t','i','o','n'}, {'g','r','a','b'}, {'m','a','r','s'}, {'d','e','l','t','a'}, {'s','i','g','m','a'}, {'f','l','o','w'}, {'l','u','c','k','y'}};

    // get word choice from user.
    cout << "Choose Word (1-10): ";

    cin >> s;

    s--;

    // get length of the word choosen.
    for(int i = 0; i < 8; i++)
    {
        if(guess[s][i])
        {
            length++;
        }
    }

    Word word(length,guess[s]);

}

