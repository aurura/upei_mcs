#!/bin/bash
# compile c++ file using c++
# Author Gideon Landry
# Date April 12, 2021

echo "Compiling..."

c++ Hangman.c++

echo "Running"

./a.out

echo "cleaning"

rm a.out

