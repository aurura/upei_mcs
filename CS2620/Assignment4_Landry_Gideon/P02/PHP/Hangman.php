<?php
/**
    play hangman

    Author: Gideon Landry
    Date: April 6, 2021
 */

$word = (int)readline('Select word  (1-10): ');
guess($word - 1);

// 
function guess($word)
{
    $guessWord = selectWord($word);
    $guesses = array();
    $correct = 0;
    $incorrect = 0;

    for($i = 0; $i < sizeof($guessWord); $i++)
    {
        array_push($guesses,"*");
    }
    while($correct < sizeof($guessWord) && $incorrect < 6)
    {
	foreach($guesses as $c)
	{
	    echo $c;
	}
        $char = readline('Enter Letter: ');
        $found = false;
        for($i = 0; $i < sizeof($guessWord); $i++)
        {
            if($guessWord[$i] == $char)
            {
                $guesses[$i] = $char;
                $correct++;
                $found = true;
            }
        }
        if(!$found)
        {
            $incorrect++;
        }
    }
}

// words
function selectWord($word)
{
   $words = array(array('h','e','l','l','o'), array('w','o','r','l','d'), array('s','t','a','r'), array('f','u','n','c','t','i','o','n'), array('g','r','a','b'), array('m','a','r','s'),array('d','e','l','t','a'),array('s','i','g','m','a'),array('f','l','o','w'),array('l','u','c','k','y'));
   return $words[$word];
}

?>
